/*
 * @file         SisypheResource.java
 * @creation     1999-08-02
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sisyphe;
import com.memoire.bu.BuResource;

import org.fudaa.fudaa.ressource.FudaaResource;
/**
 * SisypheResource
 * Le gestionnaire de ressources de Sisyphe.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author       Mickael Rubens
 */
public class SisypheResource extends BuResource {
  public final static String CAS= "CAS";
  public final static String COND= "COND";
  public final static String GEO= "GEO";
  public final static String PRE= "PRE";
  public final static String PRS= "PRS";
  public final static String REF= "REF";
  public final static String F= "F";
  public final static SisypheResource SISYPHE=
    new SisypheResource(FudaaResource.FUDAA);
  protected SisypheResource(final BuResource _parent) {
    setParent(_parent);
  }
}
