/*
 * @file         SisypheImplementation.java
 * @creation     1999-08-02
 * @modification $Date: 2007-05-04 14:01:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sisyphe;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.Map;

import javax.swing.JOptionPane;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.sisyphe.ICalculSisyphe;
import org.fudaa.dodico.corba.sisyphe.ICalculSisypheHelper;
import org.fudaa.dodico.corba.sisyphe.IParametresSisyphe;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheCOND;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheF;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheGEO;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheREF;

import org.fudaa.dodico.sisyphe.DCalculSisyphe;

import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * L'implementation du client Sisyphe.
 * 
 * @version $Revision: 1.15 $ $Date: 2007-05-04 14:01:06 $ by $Author: deniger $
 * @author Mickael Rubens
 */
public class SisypheImplementation extends FudaaImplementation {
  private SisypheAssistant assistant_;
  public final static boolean IS_APPLICATION = true;
  public static ICalculSisyphe SERVEUR_SISYPHE = null;
  public static IConnexion CONNEXION_SISYPHE = null;
  protected IParametresSisyphe sisypheParams;
  protected static BuInformationsSoftware isSisyphe_ = new BuInformationsSoftware();
  protected static BuInformationsDocument idSisyphe_ = new BuInformationsDocument();
  protected BuTaskView taches_;
  protected FudaaProjet projet_;
  protected BuPreferencesFrame preferences_;
  protected SisypheFilleParametres fp_;
  static {
    isSisyphe_.name = "Sisyphe";
    isSisyphe_.version = "0.25";
    isSisyphe_.date = "08-Dec-1999";
    isSisyphe_.rights = "Tous droits r�serv�s. CETMEF (c)1999";
    isSisyphe_.contact = "rubensmi@etu.utc.fr";
    isSisyphe_.license = "GPL2";
    isSisyphe_.languages = "fr";
    isSisyphe_.logo = SisypheResource.SISYPHE.getIcon("sisyphelogo");
    isSisyphe_.banner = SisypheResource.SISYPHE.getIcon("sisyphebanner");
    isSisyphe_.http = "http://172.17.250.86/~desnoix/fudaa";
    isSisyphe_.update = "http://172.17.250.86/~desnoix/fudaa/deltas/";
    isSisyphe_.man = "http://172.17.250.86/~desnoix/fudaa/manuels/sisyphe/";
    isSisyphe_.authors = new String[] { "Mickael Rubens" };
    isSisyphe_.contributors = new String[] { "Equipes Dodico, Ebli et Fudaa", "Mickael Rubens" };
    BuPrinter.INFO_LOG = isSisyphe_;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return isSisyphe_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isSisyphe_;
  }

  public BuInformationsDocument getInformationsDocument() {
    return idSisyphe_;
  }

  public void init() {
    super.init();
    preferences_ = null;
    fp_ = null;
    projet_ = null;
    try {
      final BuInformationsSoftware infSoft = getInformationsSoftware();
      setTitle(infSoft.name + " " + infSoft.version);
      final BuMenuBar mb = getMainMenuBar();
      mb.addActionListener(this);
      mb.addMenu(buildCalculMenu(IS_APPLICATION));
      ((BuMenu) mb.getMenu("IMPORTER")).addMenuItem("Code Sisyphe", "IMPORTSIS", false);
      final BuToolBar tb = getMainToolBar();
      tb.addToolButton("Calculer", "CALCULER", false);
      tb.addToolButton("Connecter", "CONNECTER", FudaaResource.FUDAA.getIcon("connecter"), true);
      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("IMPORTER", true);
      setEnabledForAction("IMPRIMER", false);
      setEnabledForAction("PREFERENCE", true);
      setEnabledForAction("AIDE_INDEX", true);
      setEnabledForAction("AIDE_ASSISTANT", true);
      assistant_ = new SisypheAssistant();
      assistant_.setBorder(null);
      final BuScrollPane sp = new BuScrollPane(taches_);
      sp.setPreferredSize(new Dimension(150, 80));
      getMainPanel().getRightColumn().addToggledComponent("Assistant", "ASSISTANT", assistant_, this);
      getMainPanel().getRightColumn().addToggledComponent(BuResource.BU.getString("Taches"), "TACHE", sp, this);
      getMainPanel().setLogo(infSoft.logo);
      getMainPanel().setTaskView(taches_);
    } catch (final Throwable t) {
      System.err.println("$$$ " + t);
      t.printStackTrace();
    }
  }

  public void start() {
    super.start();
    // about();
    final BuInformationsSoftware infSoft = getInformationsSoftware();
    // (new SisypheConnexion(this)).start();
    assistant_.changeAttitude(BuAssistant.PAROLE, "Bienvenue !\n" + isSisyphe_.name + " " + isSisyphe_.version);
    final BuMainPanel mp = getMainPanel();
    projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("sis"));
    mp.doLayout();
    mp.validate();
    assistant_.addEmitters((Container) getApp());
    assistant_.changeAttitude(BuAssistant.ATTENTE, "Vous pouvez cr�er un\nnouveau projet Sisyphe\nou en ouvrir un");
    // Application des pr�f�rences
    BuPreferences.BU.applyOn(this);
    // Preferences Sisyphe
    SisyphePreferences.SISYPHE.applyOn(this);
    getMainPanel().setLogo(infSoft.logo);
  }

  // Menu calcul
  protected BuMenu buildCalculMenu(final boolean _app) {
    final BuMenu r = new BuMenu("Calcul", "CALCUL");
    r.addMenuItem("Parametres", "PARAMETRE", false);
    r.addMenuItem("Calculer", "CALCULER", false);
    return r;
  }

  // Actions
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    if (action.equals("AIDE_INDEX")) {
      aideIndex();
    } else if (action.equals("OUVRIR")) {
      ouvrir();
    } else if (action.equals("FERMER")) {
      fermer();
    } else if (action.equals("ASSISTANT")) {
      getMainPanel().getRightColumn().toggleComponent(action);
      setCheckedForAction(action, getMainPanel().getRightColumn().isToggleComponentVisible(action));
    } else if (action.equals("CALCULER")) {
      calculer();
    } else if (action.equals("CREER")) {
      creer();
    } else if (action.equals("ENREGISTRER")) {
      enregistrer();
    } else if (action.equals("ENREGISTRERSOUS")) {
      enregistrerSous();
    } else if (action.equals("PREFERENCE")) {
      preferences();
    } else if (action.equals("PARAMETRE")) {
      parametre();
    } else if (action.startsWith("IMPORTSIS")) {
      importer(action.length() > 9 ? action.substring(9) : null);
    } else if (action.equals("ESFICHIER")) {
      fp_.parametrerESFichiers();
    } else if (action.equals("ESGENERALITES")) {
      fp_.parametrerESGeneralites();
    } else if (action.equals("ESGRAPHLIST")) {
      fp_.parametrerESGraphlist();
    } else if (action.equals("SOLVEUR")) {
      fp_.parametrerSolveur();
    } else if (action.equals("CONSTPHYS")) {
      fp_.parametrerConstPhys();
    } else if (action.equals("TRANSSOLID")) {
      fp_.parametrerTransSolid();
    } else if (action.equals("PARAMNUM")) {
      fp_.parametrerParamNum();
    } else {
      super.actionPerformed(_evt);
    }
  }

  // Methodes privees
  protected void ouvrir() {
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.ouvrir();
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si ouverture de projet echouee
      setEnabledForAction("PARAMETRE", false);
      setEnabledForAction("IMPORTSIS", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("CALCULER", false);
    } else {
      if (fp_ != null) {
        fp_.updatePanels(projet_);
      }
      new BuDialogMessage(getApp(), isSisyphe_, "Param�tres charg�s").activate();
      setEnabledForAction("PARAMETRE", true);
      setEnabledForAction("IMPORTSIS", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      if (isConnected()) {
        setEnabledForAction("CALCULER", true);
      } else {
        setEnabledForAction("CALCULER", false);
      }
      setTitle(projet_.getFichier());
    }
  }

  protected void fermer() {
    if (new BuDialogConfirmation(getApp(), isSisyphe_, "Voulez-vous enregistrer votre projet?").activate() == JOptionPane.YES_OPTION) {
      enregistrer();
    }
    final BuDesktop dk = getMainPanel().getDesktop();
    if (fp_ != null) {
      try {
        fp_.setClosed(true);
        fp_.setSelected(false);
      } catch (final PropertyVetoException e) {}
      dk.removeInternalFrame(fp_);
      fp_ = null;
    }
    projet_.fermer();
    dk.repaint();
    setEnabledForAction("PARAMETRE", false);
    setEnabledForAction("IMPORTSIS", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("CALCULER", false);
    setTitle("Sisyphe");
  }

  protected void creer() {
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.creer();
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si creation de projet annulee
    } else { // nouveau projet cree
      if (fp_ != null) {
        fp_.updatePanels(projet_);
      }
      setEnabledForAction("PARAMETRE", true);
      setEnabledForAction("RAPPORT", true);
      setEnabledForAction("IMPORTSIS", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("CALCULER", false);
      setTitle(projet_.getFichier());
    }
  }

  protected void enregistrer() {
    if (fp_ != null) {
      final SParametresSisypheCAS paramsCas = fp_.getParametresCas();
      projet_.addParam(SisypheResource.CAS, paramsCas);
      projet_.enregistre();
    } else {
      projet_.enregistre();
    }
  }

  protected void enregistrerSous() {
    if (fp_ != null) {
      final SParametresSisypheCAS paramsCas = fp_.getParametresCas();
      projet_.addParam(SisypheResource.CAS, paramsCas);
      projet_.enregistreSous();
    } else {
      projet_.enregistreSous();
    }
    setTitle(projet_.getFichier());
  }

  protected void buiildPreferences() {
    preferences_ = new BuPreferencesFrame();
    preferences_.addTab(new BuBrowserPreferencesPanel(this));
    preferences_.addTab(new BuDesktopPreferencesPanel(this));
    preferences_.addTab(new BuLanguagePreferencesPanel(this));
  }

  protected void importer(final String _sisypheId) {
    final SisypheImport si = new SisypheImport(getApp());
    if (_sisypheId != null) {
      si.onlyEnable(_sisypheId);
    }
    si.show();
    String tmp;
    tmp = si.getImport(SisypheResource.CAS);
    if (tmp != null) {
      projet_.addImport(SisypheResource.CAS, tmp);
    }
    tmp = si.getImport(SisypheResource.COND);
    if (tmp != null) {
      projet_.addImport(SisypheResource.COND, tmp);
    }
    tmp = si.getImport(SisypheResource.GEO);
    if (tmp != null) {
      projet_.addImport(SisypheResource.GEO, tmp);
    }
    tmp = si.getImport(SisypheResource.REF);
    if (tmp != null) {
      projet_.addImport(SisypheResource.REF, tmp);
    }
    tmp = si.getImport(SisypheResource.F);
    if (tmp != null) {
      projet_.addImport(SisypheResource.F, tmp);
    }
    if (fp_ != null) {
      fp_.updatePanels(projet_);
    }
    new BuDialogMessage(getApp(), isSisyphe_, "Param�tres import�s").activate();
    setEnabledForAction("CALCULER", true);
  }

  protected void parametre() {
    if (fp_ == null) {
      fp_ = new SisypheFilleParametres(this, isSisyphe_);
      if (!projet_.estVierge()) {
        fp_.updatePanels(projet_);
      }
      addInternalFrame(fp_);
    } else {
      if (fp_.isClosed()) {
        addInternalFrame(fp_);
      } else {
        activateInternalFrame(fp_);
      }
    }
    System.err.println("Commande 'parametre' activee");
  }

  protected void calculer() {
    if (fp_ != null) {
      final SParametresSisypheCAS paramsCas = fp_.getParametresCas();
      if (paramsCas == null) {
        new BuDialogError(getApp(), isSisyphe_, "parametres .CAS => \nun champ est vide ou de format incorrect ! ")
            .activate();
      } else {
        projet_.addParam(SisypheResource.CAS, paramsCas);
        if (!projet_.containsParam(SisypheResource.COND)) {
          new BuDialogError(getApp(), isSisyphe_, "il n'y a pas de param�tre Sisyphe .COND ! ").activate();
        } else if (!projet_.containsParam(SisypheResource.GEO)) {
          new BuDialogError(getApp(), isSisyphe_, "il n'y a pas de param�tre Sisyphe .GEO ! ").activate();
        } else if (!projet_.containsParam(SisypheResource.F)) {
          new BuDialogError(getApp(), isSisyphe_, "il n'y a pas de param�tre Sisyphe .F ! ").activate();
        } else {
          new BuTaskOperation(this, "Calcul", "oprCalculer").start();
        }
      }
    } else if (!projet_.containsParam(SisypheResource.COND)) {
      new BuDialogError(getApp(), isSisyphe_, "il n'y a pas de param�tre Sisyphe .COND ! ").activate();
    } else if (!projet_.containsParam(SisypheResource.GEO)) {
      new BuDialogError(getApp(), isSisyphe_, "il n'y a pas de param�tre Sisyphe .GEO ! ").activate();
    } else if (!projet_.containsParam(SisypheResource.F)) {
      new BuDialogError(getApp(), isSisyphe_, "il n'y a pas de param�tre Sisyphe .F ! ").activate();
    } else {
      new BuTaskOperation(this, "Calcul", "oprCalculer").start();
    }
  }

  public void oprCalculer() {
    if (!isConnected()) {
      new BuDialogError(getApp(), isSisyphe_, "vous n'etes pas connect� � un serveur Sisyphe ! ").activate();
      return;
    }
    setEnabledForAction("CALCULER", false);
    final BuMainPanel mp = getMainPanel();
    System.err.println("Transmission des parametres...");
    mp.setMessage("Transmission des parametres...");
    mp.setProgression(0);
    System.err.println("donnees Geo!");
    sisypheParams.parametresSisypheGEO((SParametresSisypheGEO) projet_.getParam(SisypheResource.GEO));
    System.err.println("donnees Cas!");
    sisypheParams.parametresSisypheCAS((SParametresSisypheCAS) projet_.getParam(SisypheResource.CAS));
    System.err.println("donnees Cond!");
    sisypheParams.parametresSisypheCOND((SParametresSisypheCOND) projet_.getParam(SisypheResource.COND));
    System.err.println("donnees Ref!");
    if (projet_.getParam(SisypheResource.REF) != null) {
      sisypheParams.parametresSisypheREF((SParametresSisypheREF) projet_.getParam(SisypheResource.REF));
    } // else sisypheParams.parametresSisypheREF(new SParametresSisypheREF());
    System.err.println("donnees F!");
    sisypheParams.parametresSisypheF((SParametresSisypheF) projet_.getParam(SisypheResource.F));
    System.err.println("Execution du calcul...");
    mp.setMessage("Execution du calcul...");
    mp.setProgression(20);
    try {
      SERVEUR_SISYPHE.calcul(CONNEXION_SISYPHE);
    } catch (final org.omg.CORBA.UNKNOWN u) {
      new BuDialogError(getApp(), isSisyphe_, u.toString()).activate();
      setEnabledForAction("CALCULER", true);
      return;
    }
    new BuDialogError(getApp(), isSisyphe_, "Les r�sultats du calcul se trouvent sur la machine serveur!").activate();
    setEnabledForAction("CALCULER", true);
  }

  public void setLookAndFeel(final String _laf) {
    super.setLookAndFeel(_laf);
  }

  public void aideIndex() {
    final BuInformationsSoftware infSoft = getInformationsSoftware();
    final String tm = "Allez donc faire un tour ici:\n" + infSoft.http;
    final BuDialogMessage dialogMesg = new BuDialogMessage(this, infSoft, tm);
    dialogMesg.activate();
  }

  public void exit() {
    fermer();
    super.exit();
  }

  public boolean confirmExit() {
    return true;
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
    CONNEXION_SISYPHE = null;
    SERVEUR_SISYPHE = null;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVEUR_SISYPHE, CONNEXION_SISYPHE);
    return new FudaaDodicoTacheConnexion[] { c };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculSisyphe.class };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(final Map _r) {
    final FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculSisyphe.class);
    CONNEXION_SISYPHE = c.getConnexion();
    SERVEUR_SISYPHE = ICalculSisypheHelper.narrow(c.getTache());
  }

  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    return SisyphePreferences.SISYPHE;
  }
}
