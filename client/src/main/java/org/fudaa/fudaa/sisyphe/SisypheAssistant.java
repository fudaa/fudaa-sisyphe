/*
 * @file         SisypheAssistant.java
 * @creation     1999-08-04
 * @modification $Date: 2003-11-25 10:14:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sisyphe;
import com.memoire.bu.BuAssistant;
/**
 * Assistant Sisyphe
 *
 * @version      $Revision: 1.4 $ $Date: 2003-11-25 10:14:25 $ by $Author: deniger $
 * @author       Mickael Rubens
 */
public class SisypheAssistant extends BuAssistant {}
