/*
 * @file         SisypheFilleParametres.java
 * @creation     1999-10-27
 * @modification $Date: 2007-05-04 14:01:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sisyphe;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuResource;

import org.fudaa.dodico.corba.sisyphe.SParametresLigneSisypheCOND;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheCOND;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheF;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheGEO;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheREF;

import org.fudaa.dodico.sisyphe.DParametresSisyphe;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
/**
 * SisypheFilleParametre
 * Fenetre fille permettant d'entrer les parametres.
 *
 * @version      $Revision: 1.9 $ $Date: 2007-05-04 14:01:06 $ by $Author: deniger $
 * @author       Mickael Rubens
 */
public class SisypheFilleParametres extends JInternalFrame {
  SisypheImplementation app_;
  //BuCommonInterface app_ ;
  BuInformationsSoftware is_;
  static String NUM_SERIE= "acc�der s�rie";
  static String NB_SERIES= "nombre de s�rie(s)";
  //////////////////// JTextFields CAS
  JTextField tf_hmin; // Hauteur d'eau minimale imposee
  JTextField tf_sfon; // Coefficient de frottement de Strickler sur le fond
  /////////////Parametres ES
  ////////Noms des fichiers
  JTextField tf_nomfon; // [72] Nom du fichier des fonds
  JTextField tf_nompre; // [72] Nom du fichier du calcul precedent
  JTextField tf_nomprs; // [72] Nom du fichier precedent sedimentologique
  JTextField tf_nomref; // [72] Nom du fichier de reference
  JTextField tf_nomgeo; // [72] Nom du fichier geometrie
  JTextField tf_nomf; // [72] Nom du fichier fortran
  JTextField tf_nomcas; // [72] Nom du fichier des parametres
  JTextField tf_nomcond; // [72] Nom du fichier des conditions aux limites
  JTextField tf_nomsol; // [72] Nom du fichier des resultats
  ////////Parametres Standard
  JTextField tf_bingeo; // [3] Standard du fichier de geometrie
  JTextField tf_binpre; // [3] Standard du fichier precedent
  JTextField tf_binprs; // [3] Standard du fichier precedent sedimentologique
  JTextField tf_binres; // [3] Standard du fichier resultat
  JTextField tf_binref; // [3] Standard du fichier de reference
  ////////Parametres Calcul
  JTextField tf_numver; // [72] numero de version
  JTextField tf_titca; // [72] Titre du cas etudie
  JTextField tf_bibli;
  // [72] Utilise par la procedure de lancement sur station de travail
  JCheckBoxMenuItem cbmi_perma; // Option cas permanent
  JCheckBoxMenuItem cbmi_valid; // Option validation
  JCheckBoxMenuItem cbmi_pdtvar;
  // Pas de temps variable pour avoir un nombre de courant inferieur a 0.2
  ////////Parametres Environnement
  JTextField tf_memcray;
  // [72] Place memoire (en mots de 8 octets) reservee en machine pour la realisation du calcul
  JTextField tf_pwdcray; // [72] Mot de passe associe a l'USER CRAY
  JTextField tf_tmcray;
  // [72] Temps CPU (en secondes) alloue pour la realisation du calcul
  JTextField tf_ucray; // [72] Userid CRAY de l'utilisateur
  ////////Parametres Logiciel Dessin
  JTextField tf_logpre; // logdes pour le calcul precedent
  JTextField tf_logdes;
  // Definit le logiciel graphique choisi pour la realisation des sorties graphiques
  ////////Parametres Generaux
  JCheckBoxMenuItem cbmi_bandec; // Option de calcul des bancs decouvrants
  JTextField tf_calmeth; // Determine la methode de filtrage des evenements
  JTextField tf_n1; // Coefficient de filtrage
  JTextField tf_optban; // Option de traitement des bancs decouvrants
  JTextField tf_lvect; // Longueur du vecteur de la machine
  JTextField tf_zero; // Zero du code
  JTextField tf_teta; // Coefficient d'implication du schema numerique
  JTextField tf_beta;
  // Coeeficient qui intervient dans la formule de l'effet de pente de Koch et Flokstra
  ////////Parametres Duree Calcul
  JTextField tf_delt; // Pas de temps
  JTextField tf_orghydro;
  // Ce mot cle fixe le temps auquel le calcul SISYPHE debute sauf en cas de suite de calcul car le temps initial est
  // alors lu sur le "fichier precedent sedimentologique". En non permanent, ce mot cle designe de plus le temps correspondant
  // au premier enregistrement a lire dans le "fichier du calcul precedent" contenant les donnees hydrodynamiques.
  JTextField tf_pmaree; // Periode de la maree
  JTextField tf_npas;
  // Nombre de pas de temps (utilise en regime permanent uniquement)
  JTextField tf_nmarees; // Nombre de marees ou de crues
  JTextField tf_nsous; // Nombre de sous-iterarions
  ////////Parametres ES Generalites
  JTextField tf_hydro; // Code de calcul utilise pour l'hydrodynamique
  JTextField tf_mailleur; // 1: MAILLEUR DIFFERENCES FINIES AU STANDARD LEONARD
  // 2: MAILLEUR ELEMENTS FINIS AU STANDARD PABLO 2D
  // 3: MAILLEUR ELEMENTS FINIS AU STANDARD SELAFIN
  ////////Parametres ES GraphList
  JTextField tf_sortis; // [72] Variables pour les sorties graphiques
  JTextField tf_varim; // [72] Variables a imprimer
  JCheckBoxMenuItem cbmi_bilma; // Option de calcul du bilan de masse
  JTextField tf_leopr; // Periode de sortie graphique
  JTextField tf_lispr; // Periode de sortie listing
  ////////Parametres Solveur
  JCheckBoxMenuItem cbmi_lump;
  // Si oui, on effectue du mass-lumping sur la cote du fond
  JTextField tf_solveur;
  // Permet de choisir le solveur utilise pour la resolution de l'equation de continuite sur le fond
  JTextField tf_optsolv;
  // Si le solveur est GMRES (7) le mot cle est la dimension de l'espace de KRILOV (valeurs conseillees entre 2 et 15).
  JTextField tf_precon;
  // Permet de preconditionner le systeme lineaire afin d'accelerer la convergence lors de sa resolution
  JTextField tf_maxit;
  // Les algorithmes utilises pour la resolution de l'etape de propagation etant iteratifs; il est necessaire de limiter
  // le nombre d'iterations autorisees.
  JTextField tf_psolveur;
  // Precision demandee pour la resolution du systeme
  ////////Parametres Transport Solide
  JTextField tf_icf; // Type de formule de transport solide
  JTextField tf_rc; // Rapport d'evolution critique
  JTextField tf_dm; // Diametre moyen des grains
  JTextField tf_ac; // Parametre de Schields
  JTextField tf_xkv; // Coefficient fonction de la porosite
  JTextField tf_optfond;
  // Ce parametre permet de fixer la methode retenue pour traiter les fonds non erodables
  ////////Parametres Constantes Physiques
  JTextField tf_xmve; // Masse volumique de l'eau
  JTextField tf_xmvs; // Masse volumique du sediment
  JTextField tf_grav; // Acceleration de la pesanteur
  JTextField tf_vce; // Viscosite cinematique de l'eau
  BuButton b_esFichiers;
  BuButton b_esGeneralites;
  BuButton b_esGraphList;
  BuButton b_paramNum;
  BuButton b_transSolid;
  BuButton b_constPhys;
  BuButton b_solveur;
  SParametresSisypheCAS paramsCAS_;
  //////////////////// JTextFields COND
  // Donnees propres a TELEMAC-2D
  JTextField tf_X1;
  JTextField tf_X2;
  JTextField tf_X3;
  JTextField tf_X4;
  JTextField tf_X5;
  JTextField tf_X6;
  JTextField tf_X7;
  JTextField tf_X10;
  JTextField tf_X11;
  // donne le type de condition � la limite sur les frontieres
  // KENT: (val numerique 5)    Evolution du fond imposee a une frontiere liquide EBOR
  // KSORT: (val numerique 4)    Evolution libre a une fontiere liquide
  // KLOG ou KDAH: (2 ou 0)    Paroie solide
  JTextField tf_LIEBOR;
  JTextField tf_EBOR;
  // donne la valeur des evolutions sedimentaires quand elles sont imposees
  JTextField tf_N; // numero global du point du bord
  JTextField tf_K; // numero du point dans la numerotation des points du bord
  JLabel lb_importCOND;
  JButton bt_importCOND;
  JButton bt_paramCOND;
  JButton bt_suivantCOND;
  JButton bt_precedentCOND;
  JButton bt_accederCOND;
  JButton bt_validerCOND;
  JLabel lb_serieEnCours;
  JTabbedPane tpCond;
  JPanel pnCond;
  JPanel pnCondBis;
  JPanel pnCondSeries;
  JPanel pnCondGest;
  JInternalFrame fdSeries;
  JTextField jtf;
  int series= 0;
  int serieEnCours= 1;
  SParametresSisypheCOND _paramsCOND;
  // JComponents GEO
  JLabel lb_importGEO;
  JButton bt_importGEO;
  // JComponents PRE
  //JLabel lb_importPRE;
  //JButton bt_importPRE;
  // JComponents PRS
  //  JLabel lb_importPRS;
  //  JButton bt_importPRS;
  // JComponents REF
  JLabel lb_importREF;
  JButton bt_importREF;
  // JComponents F
  JLabel lb_importF;
  JButton bt_importF;
  JComponent content_;
  JInternalFrame if_paramNum;
  JInternalFrame if_esFichiers;
  JInternalFrame if_esGeneralites;
  JInternalFrame if_esGraphlist;
  JInternalFrame if_solveur;
  JInternalFrame if_transSolid;
  JInternalFrame if_constPhys;
  public SisypheFilleParametres(
    final SisypheImplementation _appli,
    final BuInformationsSoftware _is) {
    super("", false, true, false, true);
    int n;
    app_= _appli;
    is_= _is;
    //  Parametres Sisyphe CAS
    final JPanel pnCas= new JPanel();
    final BuGridLayout loCas= new BuGridLayout();
    loCas.setColumns(3);
    loCas.setHgap(10);
    loCas.setVgap(5);
    pnCas.setLayout(loCas);
    pnCas.setBorder(new EmptyBorder(5, 5, 5, 5));
    paramsCAS_= DParametresSisyphe.initDefaut();
    n= 0;
    tf_hmin= new JTextField(15);
    tf_hmin.setName("hauteur eau min");
    tf_sfon= new JTextField(15);
    tf_sfon.setName("coeff strickler");
    tf_nomfon= new JTextField(15);
    tf_nompre= new JTextField(15);
    tf_nomprs= new JTextField(15);
    tf_nomref= new JTextField(15);
    tf_nomgeo= new JTextField(15);
    tf_nomf= new JTextField(15);
    tf_nomcas= new JTextField(15);
    tf_nomcond= new JTextField(15);
    tf_nomsol= new JTextField(15);
    tf_bingeo= new JTextField(15);
    tf_binpre= new JTextField(15);
    tf_binprs= new JTextField(15);
    tf_binres= new JTextField(15);
    tf_binref= new JTextField(15);
    tf_nomfon.setName("fichier des fonds");
    tf_nompre.setName("fichier du calcul precedent");
    tf_nomprs.setName("fichier precedent sedimentologique");
    tf_nomref.setName("fichier de reference");
    tf_nomgeo.setName("fichier geometrie");
    tf_nomf.setName("fichier fortran");
    tf_nomcas.setName("fichier des parametres");
    tf_nomcond.setName("fichier des conditions aux limites");
    tf_nomsol.setName("fichier des resultats");
    tf_bingeo.setName("fichier de geometrie");
    tf_binpre.setName("fichier precedent");
    tf_binprs.setName("fichier precedent sedimentologique");
    tf_binres.setName("fichier resultat");
    tf_binref.setName("fichier de reference");
    tf_numver= new JTextField(15);
    tf_titca= new JTextField(15);
    tf_bibli= new JTextField(15);
    cbmi_perma= new JCheckBoxMenuItem("false", false);
    cbmi_perma.setActionCommand("CHOIXPERMA");
    cbmi_perma.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        cbmi_perma.setText(Boolean.valueOf(cbmi_perma.getState()).toString());
        paramsCAS_.esGen.calcul.perma= cbmi_perma.getState();
      }
    });
    cbmi_valid= new JCheckBoxMenuItem("false", false);
    cbmi_valid.setActionCommand("CHOIXVALID");
    cbmi_valid.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        cbmi_valid.setText(Boolean.valueOf(cbmi_valid.getState()).toString());
        paramsCAS_.esGen.calcul.valid= cbmi_valid.getState();
      }
    });
    cbmi_pdtvar= new JCheckBoxMenuItem("false", false);
    cbmi_pdtvar.setActionCommand("CHOIXPDTVAR");
    cbmi_pdtvar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        cbmi_pdtvar.setText(Boolean.valueOf(cbmi_pdtvar.getState()).toString());
        paramsCAS_.esGen.calcul.pdtvar= cbmi_pdtvar.getState();
      }
    });
    tf_memcray= new JTextField(15);
    tf_pwdcray= new JTextField(15);
    tf_tmcray= new JTextField(15);
    tf_ucray= new JTextField(15);
    tf_logpre= new JTextField(15);
    tf_logdes= new JTextField(15);
    tf_hydro= new JTextField(15);
    tf_mailleur= new JTextField(15);
    tf_numver.setName("numero version");
    tf_titca.setName("titre cas etudie");
    tf_bibli.setName("bibli");
    tf_memcray.setName("memCRAY");
    tf_pwdcray.setName("pwdCRAY");
    tf_tmcray.setName("tmCRAY");
    tf_ucray.setName("uCRAY");
    tf_logpre.setName("logpre");
    tf_logdes.setName("logdes");
    tf_hydro.setName("Code de calcul hydrodynamique");
    tf_mailleur.setName("Mailleur");
    tf_sortis= new JTextField(15);
    tf_varim= new JTextField(15);
    cbmi_bilma= new JCheckBoxMenuItem("false", false);
    cbmi_bilma.setActionCommand("CHOIXBILMA");
    cbmi_bilma.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        cbmi_bilma.setText(Boolean.valueOf(cbmi_bilma.getState()).toString());
        paramsCAS_.esGrLst.bilma= cbmi_bilma.getState();
      }
    });
    tf_leopr= new JTextField(15);
    tf_lispr= new JTextField(15);
    tf_sortis.setName("Variables sorties graphiques");
    tf_varim.setName("Variables a imprimer");
    tf_leopr.setName("Periode sortie graphique");
    tf_lispr.setName("Periode sortie listing");
    cbmi_lump= new JCheckBoxMenuItem("false", false);
    cbmi_lump.setActionCommand("CHOIXLUMP");
    cbmi_lump.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        cbmi_lump.setText(Boolean.valueOf(cbmi_lump.getState()).toString());
        paramsCAS_.solveur.lump= cbmi_lump.getState();
      }
    });
    tf_solveur= new JTextField(15);
    tf_optsolv= new JTextField(15);
    tf_precon= new JTextField(15);
    tf_maxit= new JTextField(15);
    tf_psolveur= new JTextField(15);
    tf_solveur.setName("solveur utilise");
    tf_optsolv.setName("dimension de l'espace de KRILOV ");
    tf_precon.setName("preconditionnement du systeme lineaire");
    tf_maxit.setName("nombre iterations autorisees");
    tf_psolveur.setName("Precision pour la resolution");
    tf_xmve= new JTextField(15);
    tf_xmvs= new JTextField(15);
    tf_grav= new JTextField(15);
    tf_vce= new JTextField(15);
    tf_xmve.setName("Masse volumique eau");
    tf_xmvs.setName("Masse volumique sediment");
    tf_grav.setName("Acceleration pesanteur");
    tf_vce.setName("Viscosite cinematique eau");
    tf_icf= new JTextField(15);
    tf_rc= new JTextField(15);
    tf_dm= new JTextField(15);
    tf_ac= new JTextField(15);
    tf_xkv= new JTextField(15);
    tf_optfond= new JTextField(15);
    tf_icf.setName("formule transport solide");
    tf_rc.setName("rapport evolution critique");
    tf_dm.setName("diametre moyen grains");
    tf_ac.setName("parametre Schields");
    tf_xkv.setName("coefficient porosite");
    tf_optfond.setName("methode retenue traiter fonds non erodables");
    cbmi_bandec= new JCheckBoxMenuItem("false", false);
    cbmi_bandec.setActionCommand("CHOIXBANDEC");
    cbmi_bandec.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        cbmi_bandec.setText(Boolean.valueOf(cbmi_bandec.getState()).toString());
        paramsCAS_.paramNum.general.bandec= cbmi_bandec.getState();
      }
    });
    tf_calmeth= new JTextField(15);
    tf_n1= new JTextField(15);
    tf_optban= new JTextField(15);
    tf_lvect= new JTextField(15);
    tf_zero= new JTextField(15);
    tf_teta= new JTextField(15);
    tf_beta= new JTextField(15);
    tf_delt= new JTextField(15);
    tf_orghydro= new JTextField(15);
    tf_pmaree= new JTextField(15);
    tf_npas= new JTextField(15);
    tf_nmarees= new JTextField(15);
    tf_nsous= new JTextField(15);
    tf_calmeth.setName("methode filtrage evenements");
    tf_n1.setName("Coefficient filtrage");
    tf_optban.setName("Option traitement bancs decouvrants");
    tf_lvect.setName("Longueur vecteur machine");
    tf_zero.setName("Zero du code");
    tf_teta.setName("Coefficient implication schema numerique");
    tf_beta.setName("Coefficient formule effet pente Koch Flokstra");
    tf_delt.setName("pas de temps");
    tf_orghydro.setName("temps de debut du calcul");
    tf_pmaree.setName("periode maree");
    tf_npas.setName("nombre pas de temps");
    tf_nmarees.setName("nombre marees");
    tf_nsous.setName("nombre de sous-iterarions");
    b_esFichiers=
      new BuButton(
        BuLib.loadCommandIcon("CONFIGURER"),
        BuResource.BU.getString("Parametrer"));
    b_esGeneralites=
      new BuButton(
        BuLib.loadCommandIcon("CONFIGURER"),
        BuResource.BU.getString("Parametrer"));
    b_esGraphList=
      new BuButton(
        BuLib.loadCommandIcon("CONFIGURER"),
        BuResource.BU.getString("Parametrer"));
    b_paramNum=
      new BuButton(
        BuLib.loadCommandIcon("CONFIGURER"),
        BuResource.BU.getString("Parametrer"));
    b_transSolid=
      new BuButton(
        BuLib.loadCommandIcon("CONFIGURER"),
        BuResource.BU.getString("Parametrer"));
    b_constPhys=
      new BuButton(
        BuLib.loadCommandIcon("CONFIGURER"),
        BuResource.BU.getString("Parametrer"));
    b_solveur=
      new BuButton(
        BuLib.loadCommandIcon("CONFIGURER"),
        BuResource.BU.getString("Parametrer"));
    b_esFichiers.setActionCommand("ESFICHIER");
    b_esGeneralites.setActionCommand("ESGENERALITES");
    b_esGraphList.setActionCommand("ESGRAPHLIST");
    b_paramNum.setActionCommand("PARAMNUM");
    b_transSolid.setActionCommand("TRANSSOLID");
    b_constPhys.setActionCommand("CONSTPHYS");
    b_solveur.setActionCommand("SOLVEUR");
    b_esFichiers.addActionListener(app_);
    b_esGeneralites.addActionListener(app_);
    b_esGraphList.addActionListener(app_);
    b_paramNum.addActionListener(app_);
    b_transSolid.addActionListener(app_);
    b_constPhys.addActionListener(app_);
    b_solveur.addActionListener(app_);
    pnCas.add(new JLabel("ES Fichiers : ", SwingConstants.LEFT), n++);
    pnCas.add(new JLabel("", SwingConstants.CENTER), n++);
    pnCas.add(b_esFichiers, n++);
    pnCas.add(new JLabel("ES Generalites : ", SwingConstants.LEFT), n++);
    pnCas.add(new JLabel("", SwingConstants.CENTER), n++);
    pnCas.add(b_esGeneralites, n++);
    pnCas.add(new JLabel("ES GraphList : ", SwingConstants.LEFT), n++);
    pnCas.add(new JLabel("", SwingConstants.CENTER), n++);
    pnCas.add(b_esGraphList, n++);
    pnCas.add(new JLabel("Parametres Numeriques : ", SwingConstants.LEFT), n++);
    pnCas.add(new JLabel("", SwingConstants.CENTER), n++);
    pnCas.add(b_paramNum, n++);
    pnCas.add(new JLabel("Transport Solide : ", SwingConstants.LEFT), n++);
    pnCas.add(new JLabel("", SwingConstants.CENTER), n++);
    pnCas.add(b_transSolid, n++);
    pnCas.add(new JLabel("Constantes Physiques : ", SwingConstants.LEFT), n++);
    pnCas.add(new JLabel("", SwingConstants.CENTER), n++);
    pnCas.add(b_constPhys, n++);
    pnCas.add(new JLabel("Solveur : ", SwingConstants.LEFT), n++);
    pnCas.add(new JLabel("", SwingConstants.CENTER), n++);
    pnCas.add(b_solveur, n++);
    pnCas.add(new JLabel("Hauteur d'eau minimale imposee : "));
    pnCas.add(tf_hmin, n++);
    pnCas.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pnCas.add(new JLabel("Coefficient de Strickler sur le fond : "));
    pnCas.add(tf_sfon, n++);
    pnCas.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    //  Parametres Sisyphe COND
    tf_X1= new JTextField(15);
    tf_X2= new JTextField(15);
    tf_X3= new JTextField(15);
    tf_X4= new JTextField(15);
    tf_X5= new JTextField(15);
    tf_X6= new JTextField(15);
    tf_X7= new JTextField(15);
    tf_X10= new JTextField(15);
    tf_X11= new JTextField(15);
    tf_LIEBOR= new JTextField(15);
    tf_EBOR= new JTextField(15);
    tf_N= new JTextField(15);
    tf_K= new JTextField(15);
    pnCondSeries= new JPanel();
    pnCondGest= new JPanel();
    pnCond= new JPanel();
    pnCond.setLayout(new BorderLayout());
    pnCond.setBorder(new EmptyBorder(5, 5, 5, 5));
    tpCond= new JTabbedPane(SwingConstants.RIGHT);
    n= 0;
    lb_serieEnCours= new JLabel("s�rie n�", SwingConstants.CENTER);
    lb_importCOND= new JLabel("Il n'y a pas de param�tres Sisyphe .COND");
    bt_importCOND= new JButton("Importer");
    bt_paramCOND= new JButton("Parametrer");
    bt_importCOND.setActionCommand("IMPORTSIS" + SisypheResource.COND);
    bt_importCOND.addActionListener(_appli);
    bt_paramCOND.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        demanderSerie(NB_SERIES);
      }
    });
    pnCondBis= new JPanel();
    pnCondBis.setLayout(new BuGridLayout(3, 10, 5, true, true));
    pnCondBis.add(lb_importCOND, n++);
    pnCondBis.add(bt_importCOND, n++);
    pnCondBis.add(bt_paramCOND, n++);
    pnCond.add("North", pnCondBis);
    // Parametres Sisyphe GEO
    final JPanel pnGeo= new JPanel();
    final BuGridLayout loGEO= new BuGridLayout();
    loGEO.setColumns(1);
    loGEO.setHfilled(true);
    loGEO.setHgap(5);
    loGEO.setVgap(5);
    pnGeo.setLayout(loGEO);
    pnGeo.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    lb_importGEO= new JLabel("Il n'y a pas de param�tres Sisyphe .GEO");
    bt_importGEO= new JButton("Importer");
    bt_importGEO.setActionCommand("IMPORTSIS" + SisypheResource.GEO);
    bt_importGEO.addActionListener(_appli);
    pnGeo.add(lb_importGEO, n++);
    pnGeo.add(bt_importGEO, n++);
    // Parametres Sisyphe PRE
    /*    JPanel pnPre=new JPanel();
        BuGridLayout loPRE=new BuGridLayout();
        loPRE.setColumns(1);
        loPRE.setHfilled(true);
        loPRE.setHgap(5);
        loPRE.setVgap(5);
        pnPre.setLayout(loPRE);
        pnPre.setBorder(new EmptyBorder(5,5,5,5));
        n=0;
        lb_importPRE=new JLabel("Il n'y a pas de param�tres Sisyphe .PRE");
        bt_importPRE=new JButton("Importer");
        bt_importPRE.setActionCommand("IMPORTSIS"+SisypheResource.PRE);
        bt_importPRE.addActionListener(_appli);
        pnPre.add(lb_importPRE, n++);
        pnPre.add(bt_importPRE, n++);*/
    // Parametres Sisyphe PRS
    /*    JPanel pnPrs=new JPanel();
        BuGridLayout loPRS=new BuGridLayout();
        loPRS.setColumns(1);
        loPRS.setHfilled(true);
        loPRS.setHgap(5);
        loPRS.setVgap(5);
        pnPrs.setLayout(loPRS);
        pnPrs.setBorder(new EmptyBorder(5,5,5,5));
        n=0;
        lb_importPRS=new JLabel("Il n'y a pas de param�tres Sisyphe .PRS");
        bt_importPRS=new JButton("Importer");
        bt_importPRS.setActionCommand("IMPORTSIS"+SisypheResource.PRS);
        bt_importPRS.addActionListener(_appli);
        pnPrs.add(lb_importPRS, n++);
        pnPrs.add(bt_importPRS, n++);*/
    // Parametres Sisyphe REF
    final JPanel pnRef= new JPanel();
    final BuGridLayout loREF= new BuGridLayout();
    loREF.setColumns(1);
    loREF.setHfilled(true);
    loREF.setHgap(5);
    loREF.setVgap(5);
    pnRef.setLayout(loREF);
    pnRef.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    lb_importREF= new JLabel("Il n'y a pas de param�tres Sisyphe .REF");
    bt_importREF= new JButton("Importer");
    bt_importREF.setActionCommand("IMPORTSIS" + SisypheResource.REF);
    bt_importREF.addActionListener(_appli);
    pnRef.add(lb_importREF, n++);
    pnRef.add(bt_importREF, n++);
    // Parametres Sisyphe F
    final JPanel pnF= new JPanel();
    final BuGridLayout loF= new BuGridLayout();
    loF.setColumns(1);
    loF.setHfilled(true);
    loF.setHgap(5);
    loF.setVgap(5);
    pnF.setLayout(loF);
    pnF.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    lb_importF= new JLabel("Il n'y a pas de param�tres Sisyphe .F");
    bt_importF= new JButton("Importer");
    bt_importF.setActionCommand("IMPORTSIS" + SisypheResource.F);
    bt_importF.addActionListener(_appli);
    pnF.add(lb_importF, n++);
    pnF.add(bt_importF, n++);
    final JTabbedPane tpMain= new JTabbedPane(SwingConstants.LEFT);
    final JPanel cdCas= new JPanel();
    final JPanel cdCond= new JPanel();
    final JPanel cdGeo= new JPanel();
    //    JPanel cdPre=new JPanel();
    //    JPanel cdPrs=new JPanel();
    final JPanel cdRef= new JPanel();
    final JPanel cdF= new JPanel();
    cdCas.setLayout(new BorderLayout());
    cdCas.add("North", pnCas);
    cdGeo.setLayout(new BorderLayout());
    cdGeo.add("North", pnGeo);
    cdCond.setLayout(new BorderLayout());
    cdCond.add("North", pnCond);
    /*    cdPre.setLayout(new BorderLayout());
        cdPre.add("North", pnPre);*/
    /*    cdPrs.setLayout(new BorderLayout());
        cdPrs.add("North", pnPrs);*/
    cdRef.setLayout(new BorderLayout());
    cdRef.add("North", pnRef);
    cdF.setLayout(new BorderLayout());
    cdF.add("North", pnF);
    tpMain.addTab(" Cas ", null, cdCas, "Parametres du fichier .Cas");
    tpMain.addTab(" Cond ", null, cdCond, "Parametres du fichier .Cond");
    tpMain.addTab(" Geo ", null, cdGeo, "Parametres du fichier .Geo");
    //    tpMain.addTab(" Pre ", null, cdPre, "Parametres du fichier .Pre");
    //    tpMain.addTab(" Prs ", null, cdPrs, "Parametres du fichier .Prs");
    tpMain.addTab(" Ref ", null, cdRef, "Parametres du fichier .ref");
    tpMain.addTab(" F ", null, cdF, "Parametres du fichier .f");
    setTextesCasDefaut();
    content_= (JComponent)getContentPane();
    content_.setBorder(new EmptyBorder(5, 5, 5, 5));
    content_.add("Center", tpMain);
    setTitle("Parametres de calcul");
    setFrameIcon(BuResource.BU.getIcon("parametre"));
    setLocation(40, 40);
    pack();
  }
  // Methodes publiques
  public void demanderSerie(final String demande) {
    fdSeries= new JInternalFrame(demande, true, false);
    final JButton OK_BUTTON=
      new BuButton(
        BuLib.loadCommandIcon("VALIDER"),
        BuResource.BU.getString("Valider"));
    final JButton CANCEL_BUTTON=
      new BuButton(
        BuLib.loadCommandIcon("ANNULER"),
        BuResource.BU.getString("Annuler"));
    final JPanel pnAffichage= new JPanel();
    final JPanel pnAction= new JPanel();
    final BuHorizontalLayout lyAction= new BuHorizontalLayout();
    final JPanel pnAction1= new JPanel();
    final BorderLayout lyThis= new BorderLayout();
    final BorderLayout lyAffichage= new BorderLayout();
    pnAffichage.setLayout(lyAffichage);
    pnAffichage.setBorder(new EmptyBorder(5, 5, 5, 5));
    lyAction.setHgap(5);
    lyAction.setVfilled(false);
    pnAction.setLayout(lyAction);
    pnAction1.add(pnAction, null);
    fdSeries.getContentPane().setLayout(lyThis);
    fdSeries.getContentPane().add(pnAction1, BorderLayout.SOUTH);
    fdSeries.getContentPane().add(pnAffichage, BorderLayout.CENTER);
    final JLabel jl= new JLabel(demande + " :");
    jtf= new JTextField(20);
    pnAffichage.add("North", jl);
    pnAffichage.add("South", jtf);
    pnAction.add(OK_BUTTON, null);
    pnAction.add(CANCEL_BUTTON, null);
    OK_BUTTON.setActionCommand("SERIESCONDOK");
    OK_BUTTON.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent f) {
        if (jtf.getText() != null) {
          final int sav= (new Integer(jtf.getText())).intValue();
          if (demande == NB_SERIES) {
            series= sav;
            serieEnCours= 1;
            _paramsCOND= new SParametresSisypheCOND();
            _paramsCOND.ligne= new SParametresLigneSisypheCOND[series];
            for (int i= 0; i < series; i++) {
              _paramsCOND.ligne[i]= new SParametresLigneSisypheCOND();
            }
            _paramsCOND.nbLignes= series;
            setTextesCond();
            construireCond();
          } else {
            if (sav > series) {
              new BuDialogMessage(
                app_,
                is_,
                "la s�rie � atteindre doit etre inf�rieure � " + series + " !")
                .activate();
            } else {
              serieEnCours= sav;
              setTextesCond();
            }
          }
          fdSeries.dispose();
        }
        fdSeries.dispose();
      }
    });
    CANCEL_BUTTON.setActionCommand("SERIESCONDCANCEL");
    CANCEL_BUTTON.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent f) {
        fdSeries.dispose();
      }
    });
    app_.addInternalFrame(fdSeries);
    fdSeries.pack();
  }
  public void construireCond() {
    int n= 0;
    int m= 0;
    pnCondSeries.setLayout(new BuGridLayout(3, 10, 5, true, true));
    pnCondGest.setLayout(new BuGridLayout(1, 10, 5, true, true));
    pnCond.remove(pnCondBis);
    tf_X1.setName("X1");
    tf_X2.setName("X2");
    tf_X3.setName("X3");
    tf_X4.setName("X4");
    tf_X5.setName("X5");
    tf_X6.setName("X6");
    tf_X7.setName("X7");
    tf_X10.setName("X10");
    tf_X11.setName("X11");
    tf_LIEBOR.setName("LIEBOR");
    tf_EBOR.setName("EBOR");
    tf_N.setName("N");
    tf_K.setName("K");
    bt_suivantCOND=
      new BuButton(
        BuLib.loadCommandIcon("AVANCER"),
        BuResource.BU.getString("serie suivante"));
    bt_suivantCOND.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        enregistrerParametresCond();
        if (serieEnCours == series) {
          serieEnCours= 1;
        } else {
          serieEnCours++;
        }
        lb_serieEnCours.setText(
          "s�rie n�" + new Integer(serieEnCours).toString());
        setTextesCond();
      }
    });
    bt_precedentCOND=
      new BuButton(
        BuLib.loadCommandIcon("RECULER"),
        BuResource.BU.getString("serie precedente"));
    bt_precedentCOND.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        enregistrerParametresCond();
        if (serieEnCours == 1) {
          serieEnCours= series;
        } else {
          serieEnCours--;
        }
        lb_serieEnCours.setText(
          "s�rie n�" + new Integer(serieEnCours).toString());
        setTextesCond();
      }
    });
    bt_accederCOND=
      new BuButton(
        BuLib.loadCommandIcon("RECHERCHE"),
        BuResource.BU.getString("acc�der s�rie"));
    bt_accederCOND.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        enregistrerParametresCond();
        demanderSerie(NUM_SERIE);
      }
    });
    bt_validerCOND=
      new BuButton(
        BuLib.loadCommandIcon("VALIDER"),
        BuResource.BU.getString("Valider"));
    bt_validerCOND.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        enregistrerParametresCond();
      }
    });
    lb_serieEnCours.setText("s�rie n�" + new Integer(serieEnCours).toString());
    pnCondSeries.add(new JLabel("X1 : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_X1, n++);
    pnCondSeries.add(new JLabel("(entier)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("X2 : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_X2, n++);
    pnCondSeries.add(new JLabel("(entier)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("X3 : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_X3, n++);
    pnCondSeries.add(new JLabel("(entier)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("X4 : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_X4, n++);
    pnCondSeries.add(new JLabel("(double)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("X5 : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_X5, n++);
    pnCondSeries.add(new JLabel("(double)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("X6 : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_X6, n++);
    pnCondSeries.add(new JLabel("(double)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("X7 : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_X7, n++);
    pnCondSeries.add(new JLabel("(double)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("X10 : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_X10, n++);
    pnCondSeries.add(new JLabel("(double)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("X11 : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_X11, n++);
    pnCondSeries.add(new JLabel("(double)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("LIEBOR : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_LIEBOR, n++);
    pnCondSeries.add(new JLabel("(entier)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("EBOR : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_EBOR, n++);
    pnCondSeries.add(new JLabel("(double)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("N : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_N, n++);
    pnCondSeries.add(new JLabel("(entier)", SwingConstants.LEFT), n++);
    pnCondSeries.add(new JLabel("K : ", SwingConstants.LEFT), n++);
    pnCondSeries.add(tf_K, n++);
    pnCondSeries.add(new JLabel("(entier)", SwingConstants.LEFT), n++);
    pnCondGest.add(bt_precedentCOND, m++);
    pnCondGest.add(lb_serieEnCours, m++);
    pnCondGest.add(bt_suivantCOND, m++);
    pnCondGest.add(bt_accederCOND, m++);
    pnCondGest.add(bt_validerCOND, m++);
    pnCond.add(BorderLayout.WEST, pnCondSeries);
    pnCond.add(BorderLayout.EAST, pnCondGest);
    this.pack();
  }
  public void updatePanels(final FudaaProjet project_) {
    final SParametresSisypheGEO paramsGEO=
      (SParametresSisypheGEO)project_.getParam(SisypheResource.GEO);
    final SParametresSisypheREF paramsREF=
      (SParametresSisypheREF)project_.getParam(SisypheResource.REF);
    final SParametresSisypheF paramsF=
      (SParametresSisypheF)project_.getParam(SisypheResource.F);
    _paramsCOND=
      (SParametresSisypheCOND)project_.getParam(SisypheResource.COND);
    paramsCAS_= (SParametresSisypheCAS)project_.getParam(SisypheResource.CAS);
    setTextesCas();
    if (_paramsCOND != null) {
      series= _paramsCOND.nbLignes;
      construireCond();
      setTextesCond();
    } else {
      lb_importCOND.setText("Il n'y a pas de parametres Sisyphe .COND");
      bt_importCOND.setEnabled(true);
      bt_paramCOND.setEnabled(true);
    }
    if (paramsGEO == null) {
      lb_importGEO.setText("Il n'y a pas de parametres Sisyphe .GEO");
      bt_importGEO.setEnabled(true);
    } else {
      lb_importGEO.setText("Les parametres Sisyphe .GEO sont charg�s");
      bt_importGEO.setEnabled(false);
    }
    if (paramsREF == null) {
      lb_importREF.setText("Il n'y a pas de parametres Sisyphe .REF");
      bt_importREF.setEnabled(true);
    } else {
      lb_importREF.setText("Les parametres Sisyphe .REF sont charg�s");
      bt_importREF.setEnabled(false);
    }
    if (paramsF == null) {
      lb_importF.setText("Il n'y a pas de parametres Sisyphe .F");
      bt_importF.setEnabled(true);
    } else {
      lb_importF.setText("Les parametres Sisyphe .F sont charg�s");
      bt_importF.setEnabled(false);
    }
    lb_importGEO.revalidate();
    lb_importREF.revalidate();
    lb_importF.revalidate();
  }
  private void setTextesCasDefaut() {
    final SParametresSisypheCAS _paramsCAS= DParametresSisyphe.initDefaut();
    tf_nomfon.setText("" + _paramsCAS.esFic.noms.nomfon);
    tf_nompre.setText("" + _paramsCAS.esFic.noms.nompre);
    tf_nomprs.setText("" + _paramsCAS.esFic.noms.nomprs);
    tf_nomref.setText("" + _paramsCAS.esFic.noms.nomref);
    tf_nomgeo.setText("" + _paramsCAS.esFic.noms.nomgeo);
    tf_nomf.setText("" + _paramsCAS.esFic.noms.nomf);
    tf_nomcas.setText("" + _paramsCAS.esFic.noms.nomcas);
    tf_nomcond.setText("" + _paramsCAS.esFic.noms.nomcond);
    tf_nomsol.setText("" + _paramsCAS.esFic.noms.nomsol);
    tf_bingeo.setText("" + _paramsCAS.esFic.std.bingeo);
    tf_binpre.setText("" + _paramsCAS.esFic.std.binpre);
    tf_binprs.setText("" + _paramsCAS.esFic.std.binprs);
    tf_binres.setText("" + _paramsCAS.esFic.std.binres);
    tf_binref.setText("" + _paramsCAS.esFic.std.binref);
    tf_numver.setText("" + _paramsCAS.esGen.calcul.numver);
    tf_titca.setText("" + _paramsCAS.esGen.calcul.titca);
    tf_bibli.setText("" + _paramsCAS.esGen.calcul.bibli);
    cbmi_perma.setText("" + _paramsCAS.esGen.calcul.perma);
    cbmi_perma.setState(
      Boolean.valueOf(_paramsCAS.esGen.calcul.perma).booleanValue());
    cbmi_valid.setText("" + _paramsCAS.esGen.calcul.valid);
    cbmi_valid.setState(
      Boolean.valueOf(_paramsCAS.esGen.calcul.valid).booleanValue());
    cbmi_pdtvar.setText("" + _paramsCAS.esGen.calcul.pdtvar);
    cbmi_pdtvar.setState(
      Boolean.valueOf(_paramsCAS.esGen.calcul.pdtvar).booleanValue());
    tf_memcray.setText("" + _paramsCAS.esGen.env.memcray);
    tf_pwdcray.setText("" + _paramsCAS.esGen.env.pwdcray);
    tf_tmcray.setText("" + _paramsCAS.esGen.env.tmcray);
    tf_ucray.setText("" + _paramsCAS.esGen.env.ucray);
    tf_logpre.setText("" + _paramsCAS.esGen.logiDes.logpre);
    tf_logdes.setText("" + _paramsCAS.esGen.logiDes.logdes);
    cbmi_bandec.setText("" + _paramsCAS.paramNum.general.bandec);
    cbmi_bandec.setState(
      Boolean.valueOf(_paramsCAS.paramNum.general.bandec).booleanValue());
    tf_calmeth.setText("" + _paramsCAS.paramNum.general.calmeth);
    tf_n1.setText("" + _paramsCAS.paramNum.general.n1);
    tf_optban.setText("" + _paramsCAS.paramNum.general.optban);
    tf_lvect.setText("" + _paramsCAS.paramNum.general.lvect);
    tf_zero.setText("" + _paramsCAS.paramNum.general.zero);
    tf_teta.setText("" + _paramsCAS.paramNum.general.teta);
    tf_beta.setText("" + _paramsCAS.paramNum.general.beta);
    tf_delt.setText("" + _paramsCAS.paramNum.dureeCalc.delt);
    tf_orghydro.setText("" + _paramsCAS.paramNum.dureeCalc.orghydro);
    tf_pmaree.setText("" + _paramsCAS.paramNum.dureeCalc.pmaree);
    tf_npas.setText("" + _paramsCAS.paramNum.dureeCalc.npas);
    tf_nmarees.setText("" + _paramsCAS.paramNum.dureeCalc.nmarees);
    tf_nsous.setText("" + _paramsCAS.paramNum.dureeCalc.nsous);
    tf_hydro.setText("" + _paramsCAS.esGen.hydro);
    tf_mailleur.setText("" + _paramsCAS.esGen.mailleur);
    tf_sortis.setText("" + _paramsCAS.esGrLst.sortis);
    tf_varim.setText("" + _paramsCAS.esGrLst.varim);
    cbmi_bilma.setText("" + _paramsCAS.esGrLst.bilma);
    cbmi_bilma.setState(Boolean.valueOf(_paramsCAS.esGrLst.bilma).booleanValue());
    tf_leopr.setText("" + _paramsCAS.esGrLst.leopr);
    tf_lispr.setText("" + _paramsCAS.esGrLst.lispr);
    cbmi_lump.setText("" + _paramsCAS.solveur.lump);
    cbmi_lump.setState(Boolean.valueOf(_paramsCAS.solveur.lump).booleanValue());
    tf_solveur.setText("" + _paramsCAS.solveur.solveur);
    tf_optsolv.setText("" + _paramsCAS.solveur.optsolv);
    tf_precon.setText("" + _paramsCAS.solveur.precon);
    tf_maxit.setText("" + _paramsCAS.solveur.maxit);
    tf_psolveur.setText("" + _paramsCAS.solveur.psolveur);
    tf_icf.setText("" + _paramsCAS.transSol.icf);
    tf_rc.setText("" + _paramsCAS.transSol.rc);
    tf_dm.setText("" + _paramsCAS.transSol.dm);
    tf_ac.setText("" + _paramsCAS.transSol.ac);
    tf_xkv.setText("" + _paramsCAS.transSol.xkv);
    tf_optfond.setText("" + _paramsCAS.transSol.optfond);
    tf_xmve.setText("" + _paramsCAS.consPhys.xmve);
    tf_xmvs.setText("" + _paramsCAS.consPhys.xmvs);
    tf_grav.setText("" + _paramsCAS.consPhys.grav);
    tf_vce.setText("" + _paramsCAS.consPhys.vce);
    tf_hmin.setText("" + _paramsCAS.hmin);
    tf_sfon.setText("" + _paramsCAS.sfon);
  }
  private void setTextesCas() {
    if (paramsCAS_ != null) {
      tf_nomfon.setText("" + paramsCAS_.esFic.noms.nomfon);
      tf_nompre.setText("" + paramsCAS_.esFic.noms.nompre);
      tf_nomprs.setText("" + paramsCAS_.esFic.noms.nomprs);
      tf_nomref.setText("" + paramsCAS_.esFic.noms.nomref);
      tf_nomgeo.setText("" + paramsCAS_.esFic.noms.nomgeo);
      tf_nomf.setText("" + paramsCAS_.esFic.noms.nomf);
      tf_nomcas.setText("" + paramsCAS_.esFic.noms.nomcas);
      tf_nomcond.setText("" + paramsCAS_.esFic.noms.nomcond);
      tf_nomsol.setText("" + paramsCAS_.esFic.noms.nomsol);
      tf_bingeo.setText("" + paramsCAS_.esFic.std.bingeo);
      tf_binpre.setText("" + paramsCAS_.esFic.std.binpre);
      tf_binprs.setText("" + paramsCAS_.esFic.std.binprs);
      tf_binres.setText("" + paramsCAS_.esFic.std.binres);
      tf_binref.setText("" + paramsCAS_.esFic.std.binref);
      tf_numver.setText("" + paramsCAS_.esGen.calcul.numver);
      tf_titca.setText("" + paramsCAS_.esGen.calcul.titca);
      tf_bibli.setText("" + paramsCAS_.esGen.calcul.bibli);
      cbmi_perma.setText("" + paramsCAS_.esGen.calcul.perma);
      cbmi_perma.setState(
        Boolean.valueOf(paramsCAS_.esGen.calcul.perma).booleanValue());
      cbmi_valid.setText("" + paramsCAS_.esGen.calcul.valid);
      cbmi_valid.setState(
        Boolean.valueOf(paramsCAS_.esGen.calcul.valid).booleanValue());
      cbmi_pdtvar.setText("" + paramsCAS_.esGen.calcul.pdtvar);
      cbmi_pdtvar.setState(
        Boolean.valueOf(paramsCAS_.esGen.calcul.pdtvar).booleanValue());
      tf_memcray.setText("" + paramsCAS_.esGen.env.memcray);
      tf_pwdcray.setText("" + paramsCAS_.esGen.env.pwdcray);
      tf_tmcray.setText("" + paramsCAS_.esGen.env.tmcray);
      tf_ucray.setText("" + paramsCAS_.esGen.env.ucray);
      tf_logpre.setText("" + paramsCAS_.esGen.logiDes.logpre);
      tf_logdes.setText("" + paramsCAS_.esGen.logiDes.logdes);
      cbmi_bandec.setText("" + paramsCAS_.paramNum.general.bandec);
      cbmi_bandec.setState(
        Boolean.valueOf(paramsCAS_.paramNum.general.bandec).booleanValue());
      tf_calmeth.setText("" + paramsCAS_.paramNum.general.calmeth);
      tf_n1.setText("" + paramsCAS_.paramNum.general.n1);
      tf_optban.setText("" + paramsCAS_.paramNum.general.optban);
      tf_lvect.setText("" + paramsCAS_.paramNum.general.lvect);
      tf_zero.setText("" + paramsCAS_.paramNum.general.zero);
      tf_teta.setText("" + paramsCAS_.paramNum.general.teta);
      tf_beta.setText("" + paramsCAS_.paramNum.general.beta);
      tf_delt.setText("" + paramsCAS_.paramNum.dureeCalc.delt);
      tf_orghydro.setText("" + paramsCAS_.paramNum.dureeCalc.orghydro);
      tf_pmaree.setText("" + paramsCAS_.paramNum.dureeCalc.pmaree);
      tf_npas.setText("" + paramsCAS_.paramNum.dureeCalc.npas);
      tf_nmarees.setText("" + paramsCAS_.paramNum.dureeCalc.nmarees);
      tf_nsous.setText("" + paramsCAS_.paramNum.dureeCalc.nsous);
      tf_hydro.setText("" + paramsCAS_.esGen.hydro);
      tf_mailleur.setText("" + paramsCAS_.esGen.mailleur);
      tf_sortis.setText("" + paramsCAS_.esGrLst.sortis);
      tf_varim.setText("" + paramsCAS_.esGrLst.varim);
      cbmi_bilma.setText("" + paramsCAS_.esGrLst.bilma);
      cbmi_bilma.setState(paramsCAS_.esGrLst.bilma);
      tf_leopr.setText("" + paramsCAS_.esGrLst.leopr);
      tf_lispr.setText("" + paramsCAS_.esGrLst.lispr);
      cbmi_lump.setText("" + paramsCAS_.solveur.lump);
      cbmi_lump.setState(paramsCAS_.solveur.lump);
      tf_solveur.setText("" + paramsCAS_.solveur.solveur);
      tf_optsolv.setText("" + paramsCAS_.solveur.optsolv);
      tf_precon.setText("" + paramsCAS_.solveur.precon);
      tf_maxit.setText("" + paramsCAS_.solveur.maxit);
      tf_psolveur.setText("" + paramsCAS_.solveur.psolveur);
      tf_icf.setText("" + paramsCAS_.transSol.icf);
      tf_rc.setText("" + paramsCAS_.transSol.rc);
      tf_dm.setText("" + paramsCAS_.transSol.dm);
      tf_ac.setText("" + paramsCAS_.transSol.ac);
      tf_xkv.setText("" + paramsCAS_.transSol.xkv);
      tf_optfond.setText("" + paramsCAS_.transSol.optfond);
      tf_xmve.setText("" + paramsCAS_.consPhys.xmve);
      tf_xmvs.setText("" + paramsCAS_.consPhys.xmvs);
      tf_grav.setText("" + paramsCAS_.consPhys.grav);
      tf_vce.setText("" + paramsCAS_.consPhys.vce);
      tf_hmin.setText("" + paramsCAS_.hmin);
      tf_sfon.setText("" + paramsCAS_.sfon);
    }
  }
  void setTextesCond() {
    lb_serieEnCours.setText("s�rie n�" + new Integer(serieEnCours).toString());
    tf_X1.setText("" + _paramsCOND.ligne[serieEnCours - 1].X1);
    tf_X2.setText("" + _paramsCOND.ligne[serieEnCours - 1].X2);
    tf_X3.setText("" + _paramsCOND.ligne[serieEnCours - 1].X3);
    tf_X4.setText("" + _paramsCOND.ligne[serieEnCours - 1].X4);
    tf_X5.setText("" + _paramsCOND.ligne[serieEnCours - 1].X5);
    tf_X6.setText("" + _paramsCOND.ligne[serieEnCours - 1].X6);
    tf_X7.setText("" + _paramsCOND.ligne[serieEnCours - 1].X7);
    tf_X10.setText("" + _paramsCOND.ligne[serieEnCours - 1].X10);
    tf_X11.setText("" + _paramsCOND.ligne[serieEnCours - 1].X11);
    tf_LIEBOR.setText("" + _paramsCOND.ligne[serieEnCours - 1].LIEBOR);
    tf_EBOR.setText("" + _paramsCOND.ligne[serieEnCours - 1].EBOR);
    tf_N.setText("" + _paramsCOND.ligne[serieEnCours - 1].N);
    tf_K.setText("" + _paramsCOND.ligne[serieEnCours - 1].K);
  }
  public SParametresSisypheCAS getParametresCas() {
    return paramsCAS_;
  }
  public void enregistrerParametresCas(final String commande) {
    if (commande == "ESFICHIER") {
      if (tf_nomfon.getText() != null) {
        paramsCAS_.esFic.noms.nomfon= tf_nomfon.getText();
      }
      if (tf_nompre.getText() != null) {
        paramsCAS_.esFic.noms.nompre= tf_nompre.getText();
      }
      if (tf_nomprs.getText() != null) {
        paramsCAS_.esFic.noms.nomprs= tf_nomprs.getText();
      }
      paramsCAS_.esFic.noms.nomprs= tf_nomprs.getText();
      if (tf_nomref.getText() != null) {
        paramsCAS_.esFic.noms.nomref= tf_nomref.getText();
      }
      if (tf_nomgeo.getText() != null) {
        paramsCAS_.esFic.noms.nomgeo= tf_nomgeo.getText();
      }
      if (tf_nomf.getText() != null) {
        paramsCAS_.esFic.noms.nomf= tf_nomf.getText();
      }
      if (tf_nomcas.getText() != null) {
        paramsCAS_.esFic.noms.nomcas= tf_nomcas.getText();
      }
      if (tf_nomcond.getText() != null) {
        paramsCAS_.esFic.noms.nomcond= tf_nomcond.getText();
      }
      if (tf_nomsol.getText() != null) {
        paramsCAS_.esFic.noms.nomsol= tf_nomsol.getText();
      }
      if (tf_bingeo.getText() != null) {
        paramsCAS_.esFic.std.bingeo= tf_bingeo.getText();
      }
      if (tf_binpre.getText() != null) {
        paramsCAS_.esFic.std.binpre= tf_binpre.getText();
      }
      if (tf_binprs.getText() != null) {
        paramsCAS_.esFic.std.binprs= tf_binprs.getText();
      }
      if (tf_binres.getText() != null) {
        paramsCAS_.esFic.std.binres= tf_binres.getText();
      }
      if (tf_binref.getText() != null) {
        paramsCAS_.esFic.std.binref= tf_binref.getText();
      }
    } else if (commande == "ESGENERALITE") {
      if (tf_titca.getText() != null) {
        paramsCAS_.esGen.calcul.titca= tf_titca.getText();
      }
      if (tf_numver.getText() != null) {
        paramsCAS_.esGen.calcul.numver= tf_numver.getText();
      }
      if (tf_bibli.getText() != null) {
        paramsCAS_.esGen.calcul.bibli= tf_bibli.getText();
      }
      paramsCAS_.esGen.calcul.perma= cbmi_perma.getState();
      paramsCAS_.esGen.calcul.valid= cbmi_valid.getState();
      paramsCAS_.esGen.calcul.pdtvar= cbmi_pdtvar.getState();
      if (tf_ucray.getText() != null) {
        paramsCAS_.esGen.env.ucray= tf_ucray.getText();
      }
      if (tf_tmcray.getText() != null) {
        paramsCAS_.esGen.env.tmcray= tf_tmcray.getText();
      }
      if (tf_pwdcray.getText() != null) {
        paramsCAS_.esGen.env.pwdcray= tf_pwdcray.getText();
      }
      if (tf_memcray.getText() != null) {
        paramsCAS_.esGen.env.memcray= tf_memcray.getText();
      }
      if (tf_logdes.getText() != null) {
        paramsCAS_.esGen.logiDes.logdes=
          new Integer(tf_logdes.getText()).intValue();
      }
      if (tf_logpre.getText() != null) {
        paramsCAS_.esGen.logiDes.logpre=
          new Integer(tf_logpre.getText()).intValue();
      }
      if (tf_hydro.getText() != null) {
        paramsCAS_.esGen.hydro= new Integer(tf_hydro.getText()).intValue();
      }
      if (tf_mailleur.getText() != null) {
        paramsCAS_.esGen.mailleur=
          new Integer(tf_mailleur.getText()).intValue();
      }
    } else if (commande == "ESGRAPHLIST") {
      if (tf_sortis.getText() != null) {
        paramsCAS_.esGrLst.sortis= tf_sortis.getText();
      }
      if (tf_varim.getText() != null) {
        paramsCAS_.esGrLst.varim= tf_varim.getText();
      }
      paramsCAS_.esGrLst.bilma= cbmi_bilma.getState();
      if (tf_leopr.getText() != null) {
        paramsCAS_.esGrLst.leopr= new Integer(tf_leopr.getText()).intValue();
      }
      if (tf_lispr.getText() != null) {
        paramsCAS_.esGrLst.lispr= new Integer(tf_lispr.getText()).intValue();
      }
    } else if (commande == "PARAMNUM") {
      paramsCAS_.paramNum.general.bandec= cbmi_bandec.getState();
      if (tf_calmeth.getText() != null) {
        paramsCAS_.paramNum.general.calmeth=
          new Integer(tf_calmeth.getText()).intValue();
      }
      if (tf_n1.getText() != null) {
        paramsCAS_.paramNum.general.n1= new Integer(tf_n1.getText()).intValue();
      }
      if (tf_optban.getText() != null) {
        paramsCAS_.paramNum.general.optban=
          new Integer(tf_optban.getText()).intValue();
      }
      if (tf_lvect.getText() != null) {
        paramsCAS_.paramNum.general.lvect=
          new Integer(tf_lvect.getText()).intValue();
      }
      if (tf_zero.getText() != null) {
        paramsCAS_.paramNum.general.zero=
          new Double(tf_zero.getText()).doubleValue();
      }
      if (tf_teta.getText() != null) {
        paramsCAS_.paramNum.general.teta=
          new Double(tf_teta.getText()).doubleValue();
      }
      if (tf_beta.getText() != null) {
        paramsCAS_.paramNum.general.beta=
          new Double(tf_beta.getText()).doubleValue();
      }
      if (tf_delt.getText() != null) {
        paramsCAS_.paramNum.dureeCalc.delt=
          new Double(tf_delt.getText()).doubleValue();
      }
      if (tf_orghydro.getText() != null) {
        paramsCAS_.paramNum.dureeCalc.orghydro=
          new Double(tf_orghydro.getText()).doubleValue();
      }
      if (tf_pmaree.getText() != null) {
        paramsCAS_.paramNum.dureeCalc.pmaree=
          new Double(tf_pmaree.getText()).doubleValue();
      }
      if (tf_npas.getText() != null) {
        paramsCAS_.paramNum.dureeCalc.npas=
          new Integer(tf_npas.getText()).intValue();
      }
      if (tf_nmarees.getText() != null) {
        paramsCAS_.paramNum.dureeCalc.nmarees=
          new Integer(tf_nmarees.getText()).intValue();
      }
      if (tf_nsous.getText() != null) {
        paramsCAS_.paramNum.dureeCalc.nsous=
          new Integer(tf_nsous.getText()).intValue();
      }
    } else if (commande == "SOLVEUR") {
      paramsCAS_.solveur.lump= cbmi_lump.getState();
      if (tf_solveur.getText() != null) {
        paramsCAS_.solveur.solveur=
          new Integer(tf_solveur.getText()).intValue();
      }
      if (tf_optsolv.getText() != null) {
        paramsCAS_.solveur.optsolv=
          new Integer(tf_optsolv.getText()).intValue();
      }
      if (tf_precon.getText() != null) {
        paramsCAS_.solveur.precon= new Integer(tf_precon.getText()).intValue();
      }
      if (tf_maxit.getText() != null) {
        paramsCAS_.solveur.maxit= new Integer(tf_maxit.getText()).intValue();
      }
      if (tf_psolveur.getText() != null) {
        paramsCAS_.solveur.psolveur=
          new Double(tf_psolveur.getText()).doubleValue();
      }
    } else if (commande == "TRANSSOLID") {
      if (tf_icf.getText() != null) {
        paramsCAS_.transSol.icf= new Integer(tf_icf.getText()).intValue();
      }
      if (tf_rc.getText() != null) {
        paramsCAS_.transSol.rc= new Double(tf_rc.getText()).doubleValue();
      }
      if (tf_dm.getText() != null) {
        paramsCAS_.transSol.dm= new Double(tf_dm.getText()).doubleValue();
      }
      if (tf_ac.getText() != null) {
        paramsCAS_.transSol.ac= new Double(tf_ac.getText()).doubleValue();
      }
      if (tf_xkv.getText() != null) {
        paramsCAS_.transSol.xkv= new Double(tf_xkv.getText()).doubleValue();
      }
      if (tf_optfond.getText() != null) {
        paramsCAS_.transSol.optfond=
          new Integer(tf_optfond.getText()).intValue();
      }
    } else if (commande == "CONSPHYS") {
      if (tf_xmve.getText() != null) {
        paramsCAS_.consPhys.xmve= new Double(tf_xmve.getText()).doubleValue();
      }
      if (tf_xmvs.getText() != null) {
        paramsCAS_.consPhys.xmvs= new Double(tf_xmvs.getText()).doubleValue();
      }
      if (tf_grav.getText() != null) {
        paramsCAS_.consPhys.grav= new Double(tf_grav.getText()).doubleValue();
      }
      if (tf_vce.getText() != null) {
        paramsCAS_.consPhys.vce= new Double(tf_vce.getText()).doubleValue();
      }
      if (tf_hmin.getText() != null) {
        paramsCAS_.hmin= new Double(tf_hmin.getText()).doubleValue();
      }
      if (tf_sfon.getText() != null) {
        paramsCAS_.sfon= new Double(tf_sfon.getText()).doubleValue();
      }
    }
  }
  public void enregistrerParametresCond() {
    final int ligne= serieEnCours - 1;
    _paramsCOND.ligne[ligne].X1= new Integer(tf_X1.getText()).intValue();
    _paramsCOND.ligne[ligne].X2= new Integer(tf_X2.getText()).intValue();
    _paramsCOND.ligne[ligne].X3= new Integer(tf_X3.getText()).intValue();
    _paramsCOND.ligne[ligne].X4= new Double(tf_X4.getText()).doubleValue();
    _paramsCOND.ligne[ligne].X5= new Double(tf_X5.getText()).doubleValue();
    _paramsCOND.ligne[ligne].X6= new Double(tf_X6.getText()).doubleValue();
    _paramsCOND.ligne[ligne].X7= new Double(tf_X7.getText()).doubleValue();
    _paramsCOND.ligne[ligne].LIEBOR=
      new Integer(tf_LIEBOR.getText()).intValue();
    _paramsCOND.ligne[ligne].EBOR= new Double(tf_EBOR.getText()).doubleValue();
    _paramsCOND.ligne[ligne].X10= new Double(tf_X10.getText()).doubleValue();
    _paramsCOND.ligne[ligne].X11= new Double(tf_X11.getText()).doubleValue();
    _paramsCOND.ligne[ligne].N= new Integer(tf_N.getText()).intValue();
    _paramsCOND.ligne[ligne].K= new Integer(tf_K.getText()).intValue();
  }
  public SParametresSisypheCOND getParametresCond() {
    return _paramsCOND;
  }
  public void parametrerParamNum() {
    if_paramNum= new JInternalFrame("Parametres Numeriques", false, true);
    final JPanel pn_paramNum= new JPanel();
    final BuGridLayout lo_paramNum= new BuGridLayout();
    BuButton b_paramNumOK;
    BuButton b_paramNumANNULER;
    lo_paramNum.setColumns(3);
    lo_paramNum.setHgap(10);
    lo_paramNum.setVgap(5);
    pn_paramNum.setLayout(lo_paramNum);
    pn_paramNum.setBorder(new EmptyBorder(5, 5, 5, 5));
    int n= 0;
    pn_paramNum.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_paramNum.add(new JLabel("Parametres generaux", SwingConstants.CENTER), n++);
    pn_paramNum.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_paramNum.add(
      new JLabel("Option de calcul des bancs decouvrants : ", SwingConstants.LEFT));
    pn_paramNum.add(cbmi_bandec, n++);
    pn_paramNum.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_paramNum.add(
      new JLabel("Methode de filtrage des evenements : ", SwingConstants.LEFT));
    pn_paramNum.add(tf_calmeth, n++);
    pn_paramNum.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_paramNum.add(new JLabel("Coefficient de filtrage : ", SwingConstants.LEFT));
    pn_paramNum.add(tf_n1, n++);
    pn_paramNum.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_paramNum.add(
      new JLabel("Option de traitement des bancs decouvrants : ", SwingConstants.LEFT));
    pn_paramNum.add(tf_optban, n++);
    pn_paramNum.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_paramNum.add(
      new JLabel("Longueur du vecteur de la machine : ", SwingConstants.LEFT));
    pn_paramNum.add(tf_lvect, n++);
    pn_paramNum.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_paramNum.add(new JLabel("Zero du code : ", SwingConstants.LEFT));
    pn_paramNum.add(tf_zero, n++);
    pn_paramNum.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_paramNum.add(
      new JLabel(
        "Coefficient d'implication du schema numerique : ",
        SwingConstants.LEFT));
    pn_paramNum.add(tf_teta, n++);
    pn_paramNum.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_paramNum.add(
      new JLabel(
        "Coefficient de la formule d'effet de pente de Koch et Flokstra : ",
        SwingConstants.LEFT));
    pn_paramNum.add(tf_beta, n++);
    pn_paramNum.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_paramNum.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_paramNum.add(
      new JLabel("Parametres de duree du calcul", SwingConstants.CENTER),
      n++);
    pn_paramNum.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_paramNum.add(new JLabel("Pas de temps : ", SwingConstants.LEFT));
    pn_paramNum.add(tf_delt, n++);
    pn_paramNum.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_paramNum.add(new JLabel("Temps de debut du calcul : ", SwingConstants.LEFT));
    pn_paramNum.add(tf_orghydro, n++);
    pn_paramNum.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_paramNum.add(new JLabel("Periode de la maree : ", SwingConstants.LEFT));
    pn_paramNum.add(tf_pmaree, n++);
    pn_paramNum.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_paramNum.add(new JLabel("Nombre de pas de temps : ", SwingConstants.LEFT));
    pn_paramNum.add(tf_npas, n++);
    pn_paramNum.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_paramNum.add(new JLabel("Nombre de marees (ou crues) : ", SwingConstants.LEFT));
    pn_paramNum.add(tf_nmarees, n++);
    pn_paramNum.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_paramNum.add(new JLabel("Nombre de sous-iterarions : ", SwingConstants.LEFT));
    pn_paramNum.add(tf_nsous, n++);
    pn_paramNum.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    final JPanel pn_button= new JPanel();
    final FlowLayout flo_button= new FlowLayout(FlowLayout.CENTER);
    pn_button.setLayout(flo_button);
    b_paramNumOK=
      new BuButton(
        BuLib.loadCommandIcon("VALIDER"),
        BuResource.BU.getString("Valider"));
    b_paramNumANNULER=
      new BuButton(
        BuLib.loadCommandIcon("ANNULER"),
        BuResource.BU.getString("Annuler"));
    b_paramNumOK.setActionCommand("PARAMNUMOK");
    b_paramNumANNULER.setActionCommand("PARAMNUMANNULER");
    b_paramNumANNULER.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        if_paramNum.dispose();
      }
    });
    b_paramNumOK.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        enregistrerParametresCas("PARAMNUM");
        if_paramNum.dispose();
      }
    });
    pn_button.add(b_paramNumOK);
    pn_button.add(b_paramNumANNULER);
    final JComponent c= (JComponent)if_paramNum.getContentPane();
    final BorderLayout blo_paramNum= new BorderLayout(10, 10);
    c.setLayout(blo_paramNum);
    c.add(pn_paramNum, BorderLayout.NORTH);
    c.add(pn_button, BorderLayout.SOUTH);
    app_.addInternalFrame(if_paramNum);
    if_paramNum.pack();
  }
  public void parametrerTransSolid() {
    if_transSolid= new JInternalFrame("Transport Solide", false, true);
    final JPanel pn_transSolid= new JPanel();
    final BuGridLayout lo_transSolid= new BuGridLayout();
    BuButton b_transSolidOK;
    BuButton b_transSolidANNULER;
    lo_transSolid.setColumns(3);
    lo_transSolid.setHgap(10);
    lo_transSolid.setVgap(5);
    pn_transSolid.setLayout(lo_transSolid);
    pn_transSolid.setBorder(new EmptyBorder(5, 5, 5, 5));
    int n= 0;
    pn_transSolid.add(
      new JLabel("Type de formule de transport solide : ", SwingConstants.LEFT));
    pn_transSolid.add(tf_icf, n++);
    pn_transSolid.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_transSolid.add(
      new JLabel("Rapport d'evolution critique : ", SwingConstants.LEFT));
    pn_transSolid.add(tf_rc, n++);
    pn_transSolid.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_transSolid.add(new JLabel("Diametre moyen des grains : ", SwingConstants.LEFT));
    pn_transSolid.add(tf_dm, n++);
    pn_transSolid.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_transSolid.add(new JLabel("Parametre de Schields : ", SwingConstants.LEFT));
    pn_transSolid.add(tf_ac, n++);
    pn_transSolid.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_transSolid.add(
      new JLabel("Coefficient fonction de la porosite : ", SwingConstants.LEFT));
    pn_transSolid.add(tf_xkv, n++);
    pn_transSolid.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_transSolid.add(
      new JLabel(
        "Methode retenue pour traiter les fonds non erodables : ",
        SwingConstants.LEFT));
    pn_transSolid.add(tf_optfond, n++);
    pn_transSolid.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    final JPanel pn_button= new JPanel();
    final FlowLayout flo_button= new FlowLayout(FlowLayout.CENTER);
    pn_button.setLayout(flo_button);
    b_transSolidOK=
      new BuButton(
        BuLib.loadCommandIcon("VALIDER"),
        BuResource.BU.getString("Valider"));
    b_transSolidANNULER=
      new BuButton(
        BuLib.loadCommandIcon("ANNULER"),
        BuResource.BU.getString("Annuler"));
    b_transSolidOK.setActionCommand("TRANSSOLIDOK");
    b_transSolidANNULER.setActionCommand("TRANSSOLIDANNULER");
    b_transSolidANNULER.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        if_transSolid.dispose();
      }
    });
    b_transSolidOK.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        enregistrerParametresCas("TRANSSOLID");
        if_transSolid.dispose();
      }
    });
    pn_button.add(b_transSolidOK);
    pn_button.add(b_transSolidANNULER);
    final JComponent c= (JComponent)if_transSolid.getContentPane();
    final BorderLayout blo_transSolid= new BorderLayout(10, 10);
    c.setLayout(blo_transSolid);
    c.add(pn_transSolid, BorderLayout.NORTH);
    c.add(pn_button, BorderLayout.SOUTH);
    app_.addInternalFrame(if_transSolid);
    if_transSolid.pack();
  }
  public void parametrerConstPhys() {
    if_constPhys= new JInternalFrame("Constantes Physiques", false, true);
    final JPanel pn_constPhys= new JPanel();
    final BuGridLayout lo_constPhys= new BuGridLayout();
    BuButton b_constPhysOK;
    BuButton b_constPhysANNULER;
    lo_constPhys.setColumns(3);
    lo_constPhys.setHgap(10);
    lo_constPhys.setVgap(5);
    pn_constPhys.setLayout(lo_constPhys);
    pn_constPhys.setBorder(new EmptyBorder(5, 5, 5, 5));
    int n= 0;
    pn_constPhys.add(
      new JLabel("Masse volumique de l'eau : ", SwingConstants.LEFT),
      n++);
    pn_constPhys.add(tf_xmve, n++);
    pn_constPhys.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_constPhys.add(
      new JLabel("Masse volumique du sediment : ", SwingConstants.LEFT),
      n++);
    pn_constPhys.add(tf_xmvs, n++);
    pn_constPhys.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_constPhys.add(
      new JLabel("Acceleration de la pesanteur : ", SwingConstants.LEFT),
      n++);
    pn_constPhys.add(tf_grav, n++);
    pn_constPhys.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    pn_constPhys.add(
      new JLabel("Viscosite cinematique de l'eau : ", SwingConstants.LEFT),
      n++);
    pn_constPhys.add(tf_vce, n++);
    pn_constPhys.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    final JPanel pn_button= new JPanel();
    final FlowLayout flo_button= new FlowLayout(FlowLayout.CENTER);
    pn_button.setLayout(flo_button);
    b_constPhysOK=
      new BuButton(
        BuLib.loadCommandIcon("VALIDER"),
        BuResource.BU.getString("Valider"));
    b_constPhysANNULER=
      new BuButton(
        BuLib.loadCommandIcon("ANNULER"),
        BuResource.BU.getString("Annuler"));
    b_constPhysOK.setActionCommand("CONSTPHYSOK");
    b_constPhysANNULER.setActionCommand("CONSTPHYSANNULER");
    b_constPhysANNULER.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        if_constPhys.dispose();
      }
    });
    b_constPhysOK.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        enregistrerParametresCas("CONSTPHYS");
        if_constPhys.dispose();
      }
    });
    pn_button.add(b_constPhysOK);
    pn_button.add(b_constPhysANNULER);
    final JComponent c= (JComponent)if_constPhys.getContentPane();
    final BorderLayout blo_constPhys= new BorderLayout(10, 10);
    c.setLayout(blo_constPhys);
    c.add(pn_constPhys, BorderLayout.NORTH);
    c.add(pn_button, BorderLayout.SOUTH);
    app_.addInternalFrame(if_constPhys);
    if_constPhys.pack();
  }
  public void parametrerSolveur() {
    if_solveur= new JInternalFrame("Solveur", false, true);
    final JPanel pn_solveur= new JPanel();
    final BuGridLayout lo_solveur= new BuGridLayout();
    BuButton b_solveurOK;
    BuButton b_solveurANNULER;
    lo_solveur.setColumns(3);
    lo_solveur.setHgap(10);
    lo_solveur.setVgap(5);
    pn_solveur.setLayout(lo_solveur);
    pn_solveur.setBorder(new EmptyBorder(5, 5, 5, 5));
    int n= 0;
    pn_solveur.add(new JLabel("mass-lumping : ", SwingConstants.LEFT));
    pn_solveur.add(cbmi_lump, n++);
    pn_solveur.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_solveur.add(new JLabel("solveur utilise : ", SwingConstants.LEFT));
    pn_solveur.add(tf_solveur, n++);
    pn_solveur.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_solveur.add(
      new JLabel("dimension de l'espace de KRILOV : ", SwingConstants.LEFT));
    pn_solveur.add(tf_optsolv, n++);
    pn_solveur.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_solveur.add(
      new JLabel("preconditionnement du systeme lineaire : ", SwingConstants.LEFT));
    pn_solveur.add(tf_precon, n++);
    pn_solveur.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_solveur.add(
      new JLabel("nombre d'iterations autorisees : ", SwingConstants.LEFT));
    pn_solveur.add(tf_maxit, n++);
    pn_solveur.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_solveur.add(new JLabel("Precision pour la resolution : ", SwingConstants.LEFT));
    pn_solveur.add(tf_psolveur, n++);
    pn_solveur.add(new JLabel("(double)", SwingConstants.CENTER), n++);
    final JPanel pn_button= new JPanel();
    final FlowLayout flo_button= new FlowLayout(FlowLayout.CENTER);
    pn_button.setLayout(flo_button);
    b_solveurOK=
      new BuButton(
        BuLib.loadCommandIcon("VALIDER"),
        BuResource.BU.getString("Valider"));
    b_solveurANNULER=
      new BuButton(
        BuLib.loadCommandIcon("ANNULER"),
        BuResource.BU.getString("Annuler"));
    b_solveurOK.setActionCommand("SOLVEUROK");
    b_solveurANNULER.setActionCommand("SOLVEURANNULER");
    b_solveurANNULER.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        if_solveur.dispose();
      }
    });
    b_solveurOK.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        enregistrerParametresCas("SOLVEUR");
        if_solveur.dispose();
      }
    });
    pn_button.add(b_solveurOK);
    pn_button.add(b_solveurANNULER);
    final JComponent c= (JComponent)if_solveur.getContentPane();
    final BorderLayout blo_solveur= new BorderLayout(10, 10);
    c.setLayout(blo_solveur);
    c.add(pn_solveur, BorderLayout.NORTH);
    c.add(pn_button, BorderLayout.SOUTH);
    app_.addInternalFrame(if_solveur);
    if_solveur.pack();
  }
  public void parametrerESGraphlist() {
    if_esGraphlist= new JInternalFrame("ESGraphList", false, true);
    final JPanel pn_esGraphlist= new JPanel();
    final BuGridLayout lo_esGraphlist= new BuGridLayout();
    BuButton b_esGraphlistOK;
    BuButton b_esGraphlistANNULER;
    lo_esGraphlist.setColumns(3);
    lo_esGraphlist.setHgap(10);
    lo_esGraphlist.setVgap(5);
    pn_esGraphlist.setLayout(lo_esGraphlist);
    pn_esGraphlist.setBorder(new EmptyBorder(5, 5, 5, 5));
    int n= 0;
    pn_esGraphlist.add(
      new JLabel("Variables pour les sorties graphiques : ", SwingConstants.LEFT));
    pn_esGraphlist.add(tf_sortis, n++);
    pn_esGraphlist.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGraphlist.add(new JLabel("Variables a imprimer : ", SwingConstants.LEFT));
    pn_esGraphlist.add(tf_varim, n++);
    pn_esGraphlist.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGraphlist.add(
      new JLabel("Option de calcul du bilan de masse : ", SwingConstants.LEFT));
    pn_esGraphlist.add(cbmi_bilma, n++);
    pn_esGraphlist.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGraphlist.add(
      new JLabel("Periode de sortie graphique : ", SwingConstants.LEFT));
    pn_esGraphlist.add(tf_leopr, n++);
    pn_esGraphlist.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_esGraphlist.add(new JLabel("Periode de sortie listing : ", SwingConstants.LEFT));
    pn_esGraphlist.add(tf_lispr, n++);
    pn_esGraphlist.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    final JPanel pn_button= new JPanel();
    final FlowLayout flo_button= new FlowLayout(FlowLayout.CENTER);
    pn_button.setLayout(flo_button);
    b_esGraphlistOK=
      new BuButton(
        BuLib.loadCommandIcon("VALIDER"),
        BuResource.BU.getString("Valider"));
    b_esGraphlistANNULER=
      new BuButton(
        BuLib.loadCommandIcon("ANNULER"),
        BuResource.BU.getString("Annuler"));
    b_esGraphlistOK.setActionCommand("ESGRAPHLISTOK");
    b_esGraphlistANNULER.setActionCommand("ESGRAPHLISTANNULER");
    b_esGraphlistANNULER
      .addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        if_esGraphlist.dispose();
      }
    });
    b_esGraphlistOK.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        enregistrerParametresCas("ESGRAPHLIST");
        if_esGraphlist.dispose();
      }
    });
    pn_button.add(b_esGraphlistOK);
    pn_button.add(b_esGraphlistANNULER);
    final JComponent c= (JComponent)if_esGraphlist.getContentPane();
    final BorderLayout blo_esGraphlist= new BorderLayout(10, 10);
    c.setLayout(blo_esGraphlist);
    c.add(pn_esGraphlist, BorderLayout.NORTH);
    c.add(pn_button, BorderLayout.SOUTH);
    app_.addInternalFrame(if_esGraphlist);
    if_esGraphlist.pack();
  }
  public void parametrerESGeneralites() {
    if_esGeneralites= new JInternalFrame("ESGeneralites", false, true);
    final JPanel pn_esGeneralites= new JPanel();
    final BuGridLayout lo_esGeneralites= new BuGridLayout();
    BuButton b_esGeneralitesOK;
    BuButton b_esGeneralitesANNULER;
    lo_esGeneralites.setColumns(3);
    lo_esGeneralites.setHgap(10);
    lo_esGeneralites.setVgap(5);
    pn_esGeneralites.setLayout(lo_esGeneralites);
    pn_esGeneralites.setBorder(new EmptyBorder(5, 5, 5, 5));
    int n= 0;
    pn_esGeneralites.add(
      new JLabel("Code de calcul hydrodynamique : ", SwingConstants.LEFT));
    pn_esGeneralites.add(tf_hydro, n++);
    pn_esGeneralites.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("Mailleur : ", SwingConstants.LEFT));
    pn_esGeneralites.add(tf_mailleur, n++);
    pn_esGeneralites.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("Parametres Calcul", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("Numero de version : ", SwingConstants.LEFT));
    pn_esGeneralites.add(tf_numver, n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("Titre du cas etudie : ", SwingConstants.LEFT));
    pn_esGeneralites.add(tf_titca, n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("Bibli : ", SwingConstants.LEFT));
    pn_esGeneralites.add(tf_bibli, n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("Option cas permanent : ", SwingConstants.LEFT));
    pn_esGeneralites.add(cbmi_perma, n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("Option validation : ", SwingConstants.LEFT));
    pn_esGeneralites.add(cbmi_valid, n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(
      new JLabel("Pas de temps pour un nb de courant < 0.2 : ", SwingConstants.LEFT));
    pn_esGeneralites.add(cbmi_pdtvar, n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(
      new JLabel("Parametres Environnement", SwingConstants.CENTER),
      n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(
      new JLabel("Memoire reservee en machine : ", SwingConstants.LEFT));
    pn_esGeneralites.add(tf_memcray, n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("Mot de passe : ", SwingConstants.LEFT));
    pn_esGeneralites.add(tf_pwdcray, n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(
      new JLabel("Temps CPU alloue pour le calcul : ", SwingConstants.LEFT));
    pn_esGeneralites.add(tf_tmcray, n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(
      new JLabel("Userid CRAY de l'utilisateur : ", SwingConstants.LEFT));
    pn_esGeneralites.add(tf_ucray, n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("Logiciel Dessin", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(
      new JLabel(
        "Logiciel graphique pour le calcul precedent : ",
        SwingConstants.LEFT));
    pn_esGeneralites.add(tf_logpre, n++);
    pn_esGeneralites.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    pn_esGeneralites.add(
      new JLabel(
        "logiciel graphique pour les sorties graphiques : ",
        SwingConstants.LEFT));
    pn_esGeneralites.add(tf_logdes, n++);
    pn_esGeneralites.add(new JLabel("(entier)", SwingConstants.CENTER), n++);
    final JPanel pn_button= new JPanel();
    final FlowLayout flo_button= new FlowLayout(FlowLayout.CENTER);
    pn_button.setLayout(flo_button);
    b_esGeneralitesOK=
      new BuButton(
        BuLib.loadCommandIcon("VALIDER"),
        BuResource.BU.getString("Valider"));
    b_esGeneralitesANNULER=
      new BuButton(
        BuLib.loadCommandIcon("ANNULER"),
        BuResource.BU.getString("Annuler"));
    b_esGeneralitesOK.setActionCommand("ESGENERALITEOK");
    b_esGeneralitesANNULER.setActionCommand("ESGENERALITEANNULER");
    b_esGeneralitesANNULER
      .addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        if_esGeneralites.dispose();
      }
    });
    b_esGeneralitesOK.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        enregistrerParametresCas("ESGENERALITE");
        if_esGeneralites.dispose();
      }
    });
    pn_button.add(b_esGeneralitesOK);
    pn_button.add(b_esGeneralitesANNULER);
    final JComponent c= (JComponent)if_esGeneralites.getContentPane();
    final BorderLayout blo_esGeneralites= new BorderLayout(10, 10);
    c.setLayout(blo_esGeneralites);
    c.add(pn_esGeneralites, BorderLayout.NORTH);
    c.add(pn_button, BorderLayout.SOUTH);
    app_.addInternalFrame(if_esGeneralites);
    if_esGeneralites.pack();
  }
  public void parametrerESFichiers() {
    if_esFichiers= new JInternalFrame("ESFichiers", false, true);
    final JPanel pn_esFichiers= new JPanel();
    final BuGridLayout lo_esFichiers= new BuGridLayout();
    BuButton b_esFichiersOK;
    BuButton b_esFichiersANNULER;
    lo_esFichiers.setColumns(3);
    lo_esFichiers.setHgap(10);
    lo_esFichiers.setVgap(5);
    pn_esFichiers.setLayout(lo_esFichiers);
    pn_esFichiers.setBorder(new EmptyBorder(5, 5, 5, 5));
    int n= 0;
    pn_esFichiers.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Noms des fichiers", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Fichier des fonds : ", SwingConstants.LEFT), n++);
    pn_esFichiers.add(tf_nomfon, n++);
    pn_esFichiers.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esFichiers.add(
      new JLabel("Fichier du calcul precedent : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_nompre, n++);
    pn_esFichiers.add(new JLabel("(fic.pre)", SwingConstants.CENTER), n++);
    pn_esFichiers.add(
      new JLabel("Fichier precedent sedimentologique : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_nomprs, n++);
    pn_esFichiers.add(new JLabel("(fic.prs)", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Fichier de reference : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_nomref, n++);
    pn_esFichiers.add(new JLabel("(fic.ref)", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Fichier geometrie : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_nomgeo, n++);
    pn_esFichiers.add(new JLabel("(fic.geo)", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Fichier fortran : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_nomf, n++);
    pn_esFichiers.add(new JLabel("(fic.f)", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Fichier des parametres : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_nomcas, n++);
    pn_esFichiers.add(new JLabel("(fic.cas)", SwingConstants.CENTER), n++);
    pn_esFichiers.add(
      new JLabel("Fichier des conditions aux limites : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_nomcond, n++);
    pn_esFichiers.add(new JLabel("(fic.cond)", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Fichier des resultats : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_nomsol, n++);
    pn_esFichiers.add(new JLabel("(fic.sol)", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Standards des fichiers", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Fichier de geometrie : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_bingeo, n++);
    pn_esFichiers.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Fichier precedent : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_binpre, n++);
    pn_esFichiers.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esFichiers.add(
      new JLabel("Fichier precedent sedimentologique : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_binprs, n++);
    pn_esFichiers.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Fichier resultat : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_binres, n++);
    pn_esFichiers.add(new JLabel("", SwingConstants.CENTER), n++);
    pn_esFichiers.add(new JLabel("Fichier de reference : ", SwingConstants.LEFT));
    pn_esFichiers.add(tf_binref, n++);
    pn_esFichiers.add(new JLabel("", SwingConstants.CENTER), n++);
    final JPanel pn_button= new JPanel();
    final FlowLayout flo_button= new FlowLayout(FlowLayout.CENTER);
    pn_button.setLayout(flo_button);
    b_esFichiersOK=
      new BuButton(
        BuLib.loadCommandIcon("VALIDER"),
        BuResource.BU.getString("Valider"));
    b_esFichiersANNULER=
      new BuButton(
        BuLib.loadCommandIcon("ANNULER"),
        BuResource.BU.getString("Annuler"));
    b_esFichiersOK.setActionCommand("ESFICHIEROK");
    b_esFichiersANNULER.setActionCommand("ESFICHIERANNULER");
    b_esFichiersANNULER.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        if_esFichiers.dispose();
      }
    });
    b_esFichiersOK.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        enregistrerParametresCas("ESFICHIER");
        if_esFichiers.dispose();
      }
    });
    pn_button.add(b_esFichiersOK);
    pn_button.add(b_esFichiersANNULER);
    final JComponent c= (JComponent)if_esFichiers.getContentPane();
    final BorderLayout blo_esFichiers= new BorderLayout(10, 10);
    c.setLayout(blo_esFichiers);
    c.add(pn_esFichiers, BorderLayout.NORTH);
    c.add(pn_button, BorderLayout.SOUTH);
    app_.addInternalFrame(if_esFichiers);
    if_esFichiers.pack();
  }
}
