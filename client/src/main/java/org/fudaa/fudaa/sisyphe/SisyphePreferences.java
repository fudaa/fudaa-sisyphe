/*
 * @file         SisyphePreferences.java
 * @creation     1999-11-15
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sisyphe;
import com.memoire.bu.BuPreferences;
/**
 * SisyphePreferences.java
 * Preferences pour Sisyphe.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author       Mickael Rubens
 */
public class SisyphePreferences extends BuPreferences {
  public final static SisyphePreferences SISYPHE= new SisyphePreferences();
  public void applyOn(final Object _o) {
    if (!(_o instanceof SisypheImplementation)) {
      throw new RuntimeException("" + _o + " is not a SisypheImplementation.");
    //    SisypheImplementation _appli=(SisypheImplementation)_o;
    }
  }
}
