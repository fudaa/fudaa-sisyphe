/*
 * @file         SisypheApplication.java
 * @creation     1999-08-04
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sisyphe;
import com.memoire.bu.BuApplication;
/**
 * Application Sisyphe
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author       Mickael Rubens
 */
public class SisypheApplication extends BuApplication {
  public SisypheApplication() {
    super();
    setImplementation(new SisypheImplementation());
  }
}
