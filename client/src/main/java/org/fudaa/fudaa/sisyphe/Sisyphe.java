/**
 * @file         Sisyphe.java
 * @creation     1999-10-05
 * @modification $Date: 2007-01-19 13:14:23 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sisyphe;
import org.fudaa.fudaa.commun.impl.Fudaa;
/**
 * @version      $Revision: 1.9 $ $Date: 2007-01-19 13:14:23 $ by $Author: deniger $
 * @author       Mickael Rubens
 */
public class Sisyphe {
  /**
   * @param args les arg de l'appli
   */
  public static void main(final String args[]) {
    final Fudaa f = new Fudaa();
    f.launch(args, SisypheImplementation.informationsSoftware(), false);
    f.startApp(new SisypheImplementation());
  }
}
