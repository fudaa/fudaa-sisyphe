/*
 * @file         SisypheImport.java
 * @creation     1999-11-15
 * @modification $Date: 2007-01-19 13:14:23 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sisyphe;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.fudaa.commun.impl.FudaaDialogImport;
/**
 * SisypheImport.java 
 * Boite d'importation de fichiers Sisyphe.
 *
 * @version      $Revision: 1.7 $ $Date: 2007-01-19 13:14:23 $ by $Author: deniger $
 * @author       Mickael Rubens 
 */
public class SisypheImport
  extends FudaaDialogImport
  implements ActionListener, WindowListener {
  // Donnees membres privees
  private JTextField tf_cas_, tf_cond_, tf_geo_, tf_ref_, tf_f_;
  private JButton b_cas_, b_cond_, b_geo_, b_ref_, b_f_, ok_, cancel_;
  private BuGridLayout lo_sisyphe_;
  private JPanel pl_sisyphe_, pl_ok_;
  private Frame parent_;
  // Constructeur
  public SisypheImport(final BuCommonInterface _parent) {
    super(_parent instanceof Frame ? (Frame)_parent : null);
    parent_= _parent instanceof Frame ? (Frame)_parent : null;
    int n= 0;
    pl_sisyphe_= new JPanel();
    lo_sisyphe_= new BuGridLayout();
    lo_sisyphe_.setColumns(3);
    lo_sisyphe_.setVgap(5);
    lo_sisyphe_.setHgap(5);
    pl_sisyphe_.setLayout(lo_sisyphe_);
    pl_sisyphe_.add(new JLabel(".CAS: "), n++);
    tf_cas_= new JTextField();
    tf_cas_.setColumns(20);
    pl_sisyphe_.add(tf_cas_, n++);
    b_cas_= new JButton("Parcourir");
    b_cas_.setActionCommand(SisypheResource.CAS);
    b_cas_.addActionListener(this);
    pl_sisyphe_.add(b_cas_, n++);
    pl_sisyphe_.add(new JLabel(".COND: "), n++);
    tf_cond_= new JTextField();
    tf_cond_.setColumns(20);
    pl_sisyphe_.add(tf_cond_, n++);
    b_cond_= new JButton("Parcourir");
    b_cond_.setActionCommand(SisypheResource.COND);
    b_cond_.addActionListener(this);
    pl_sisyphe_.add(b_cond_, n++);
    pl_sisyphe_.add(new JLabel(".GEO: "), n++);
    tf_geo_= new JTextField();
    tf_geo_.setColumns(20);
    pl_sisyphe_.add(tf_geo_, n++);
    b_geo_= new JButton("Parcourir");
    b_geo_.setActionCommand(SisypheResource.GEO);
    b_geo_.addActionListener(this);
    pl_sisyphe_.add(b_geo_, n++);
    pl_sisyphe_.add(new JLabel(".REF: "), n++);
    tf_ref_= new JTextField();
    tf_ref_.setColumns(20);
    pl_sisyphe_.add(tf_ref_, n++);
    b_ref_= new JButton("Parcourir");
    b_ref_.setActionCommand(SisypheResource.REF);
    b_ref_.addActionListener(this);
    pl_sisyphe_.add(b_ref_, n++);
    pl_sisyphe_.add(new JLabel(".F: "), n++);
    tf_f_= new JTextField();
    tf_f_.setColumns(20);
    pl_sisyphe_.add(tf_f_, n++);
    b_f_= new JButton("Parcourir");
    b_f_.setActionCommand(SisypheResource.F);
    b_f_.addActionListener(this);
    pl_sisyphe_.add(b_f_, n++);
    pl_ok_= new JPanel();
    ok_= new JButton("Valider");
    ok_.setActionCommand("VALIDER");
    ok_.addActionListener(this);
    cancel_= new JButton("Annuler");
    cancel_.setActionCommand("ANNULER");
    cancel_.addActionListener(this);
    pl_ok_.add(ok_);
    pl_ok_.add(cancel_);
    final Container content= getContentPane();
    content.setLayout(new BorderLayout());
    content.add(pl_sisyphe_, BorderLayout.NORTH);
    content.add(pl_ok_, BorderLayout.SOUTH);
    pack();
    setResizable(false);
    if (_parent instanceof Component) {
      setLocationRelativeTo((Component)_parent);
    }
  }
  // Methodes publiques
  // Actions
  public void actionPerformed(final ActionEvent _evt) {
    final String action= _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    if (action.equals("ANNULER")) {
      //      imports_=null;
      dispose();
    } else if (action.equals("VALIDER")) {
      imports_.put(SisypheResource.CAS, tf_cas_.getText());
      imports_.put(SisypheResource.COND, tf_cond_.getText());
      imports_.put(SisypheResource.GEO, tf_geo_.getText());
      imports_.put(SisypheResource.REF, tf_ref_.getText());
      imports_.put(SisypheResource.F, tf_f_.getText());
      dispose();
    } else if (
      action.equals(SisypheResource.CAS)
        || action.equals(SisypheResource.COND)
        || action.equals(SisypheResource.GEO)
        || action.equals(SisypheResource.REF)
        || action.equals(SisypheResource.F)) {
      popSisypheFileChooser(action);
    }
  }
  public void onlyEnable(final String _fieldId) {
    System.err.println("onlyEnable :" + _fieldId + CtuluLibString.DOT);
    if (!_fieldId.equals(SisypheResource.CAS)) {
      tf_cas_.setEnabled(false);
      b_cas_.setEnabled(false);
    }
    if (!_fieldId.equals(SisypheResource.COND)) {
      tf_cond_.setEnabled(false);
      b_cond_.setEnabled(false);
    }
    if (!_fieldId.equals(SisypheResource.GEO)) {
      tf_geo_.setEnabled(false);
      b_geo_.setEnabled(false);
    }
    if (!_fieldId.equals(SisypheResource.REF)) {
      tf_ref_.setEnabled(false);
      b_ref_.setEnabled(false);
    }
    if (!_fieldId.equals(SisypheResource.F)) {
      tf_f_.setEnabled(false);
      b_f_.setEnabled(false);
    }
  }
  // Window events
  public void windowActivated(final WindowEvent e) {}
  public void windowClosed(final WindowEvent e) {}
  public void windowClosing(final WindowEvent e) {
    imports_= null;
    dispose();
  }
  public void windowDeactivated(final WindowEvent e) {}
  public void windowDeiconified(final WindowEvent e) {}
  public void windowIconified(final WindowEvent e) {}
  public void windowOpened(final WindowEvent e) {}
  // Methodes privees
  private void popSisypheFileChooser(final String _type) {
    FileDialog FDOpen= null;
    String DIROpen= null;
    String r= "";
    if (FDOpen == null) {
      FDOpen= new FileDialog(parent_, _type, FileDialog.LOAD);
    }
    if (DIROpen == null) {
      DIROpen= FDOpen.getDirectory();
    }
    if (DIROpen != null) {
      FDOpen.setDirectory(DIROpen);
    }
    FDOpen.show();
    DIROpen= FDOpen.getDirectory();
    r= FDOpen.getFile();
    if (r != null) {
      r= DIROpen + r;
    }
    /**** version Swing
    
    JFileChooser chooser=new JFileChooser();
    
    // ExtensionFileFilter filter = new ExtensionFileFilter();
    // filter.addExtension("jpg");
    // filter.addExtension("gif");
    // filter.setDescription("JPG & GIF Images");
    // chooser.setFileFilter(filter);
    
    int returnVal = chooser.showOpenDialog(this);
    if(returnVal == JFileChooser.APPROVE_OPTION)
    {
      r=chooser.getSelectedFile().getPath(); // .getName();
      System.out.println("You chose to open this file: "+r);
    }
    else r=null;
    
    ****/
    if (r != null) {
      if (_type.equals(SisypheResource.CAS)) {
        tf_cas_.setText(r);
      } else if (_type.equals(SisypheResource.COND)) {
        tf_cond_.setText(r);
      } else if (_type.equals(SisypheResource.GEO)) {
        tf_geo_.setText(r);
      } else if (_type.equals(SisypheResource.REF)) {
        tf_ref_.setText(r);
      } else if (_type.equals(SisypheResource.F)) {
        tf_f_.setText(r);
      }
    }
  }
}
