/*
 * @file         MainTestPre.java
 * @creation     1999-07-09
 * @modification $Date: 2006-09-19 14:45:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sisyphe;
import org.fudaa.dodico.corba.sisyphe.SParametresSisyphePRE;

import org.fudaa.dodico.sisyphe.DParametresSisyphe;
/**
 * @version      $Revision: 1.1 $ $Date: 2006-09-19 14:45:46 $ by $Author: deniger $
 * @author       Mickael Rubens 
 */
public final class MainTestPre {
  
  private MainTestPre() {}
  /**
   * @param args
   */
  public static void main(final String[] args) {
    System.out.println("1)Test de lecture/ecriture du fichier .PRE....");
    SParametresSisyphePRE params= new SParametresSisyphePRE();
    params=
      DParametresSisyphe.litParametresPRE(
        "/home/users/rubens/sisyphe_fic/charite/charite");
    System.out.println("Titre: " + params.titre);
    System.out.println("nbv1= " + params.NBV[0] + "  nbv2= " + params.NBV[1]);
    for (int i= 0; i < params.NBV[0]; i++) {
      System.out.println("desc" + i + ": " + params.descVariables[i]);
    }
    System.out.println("NELT: " + params.NELT + "  NNT: " + params.NNT);
    for (int i= 0; i < 4; i++) {
      System.out.println();
      System.out.print("connectivite" + i + ":");
      for (int j= 0; j < 3; j++) {
        System.out.print(" " + params.connect[i][j]);
      }
    }
    System.out.println();
    System.out.println("3)Fin du test de lecture/Ecriture du fichier .PRE...");
  }
}
