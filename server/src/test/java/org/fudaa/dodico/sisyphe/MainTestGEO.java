/*
 * @file         MainTestGEO.java
 * @creation     1999-07-09
 * @modification $Date: 2006-09-19 14:45:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sisyphe;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheGEO;

import org.fudaa.dodico.sisyphe.DParametresSisyphe;
/**
 * @version      $Revision: 1.1 $ $Date: 2006-09-19 14:45:46 $ by $Author: deniger $
 * @author       Mickael Rubens 
 */
public final class MainTestGEO {
  
  private MainTestGEO(){}
  /**
   * @param _args
   */
  public static void main(final String[] _args) {
    System.out.println("1)Test de lecture/ecriture du fichier .GEO....");
    SParametresSisypheGEO params= new SParametresSisypheGEO();
    params=
      DParametresSisyphe.litParametresGEO(
        "/home/users/rubens/sisyphe_fic/cas/ca1_3",
        1);
    System.out.println("3) copie du fichier dans test.ecritGEO");
    DParametresSisyphe.ecritParametresGEO("test.ecritGEO", params, 1);
    params= DParametresSisyphe.litParametresGEO("test.ecritGEO", 1);
    DParametresSisyphe.ecritParametresGEO("ca1_3", params, 1);
    System.out.println("3)Fin du test de lecture/Ecriture du fichier .GEO...");
  }
}
