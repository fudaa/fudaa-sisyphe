/*
 * @file         MainTestSOL.java
 * @creation
 * @modification $Date: 2006-09-19 14:45:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sisyphe;

import org.fudaa.dodico.sisyphe.DResultatsSisyphe;

/**
 * @version      $Revision: 1.1 $ $Date: 2006-09-19 14:45:47 $ by $Author: deniger $
 * @author       Mickael Rubens
 */
public final class MainTestSOL {
  
  private MainTestSOL(){}
  /**
   * @param _args
   */
  public static void main(final String[] _args) {
    System.out.println("1)Test de lecture du fichier .SOL....");
    /*SResultatsSisypheSOL params= new SResultatsSisypheSOL();
    params=*/
      DResultatsSisyphe.litResultatsSOL(
        "/home/users/rubens/sisyphe_fic/cas/ca1_3",
        1100,
        4000,
        1000);
    System.out.println("3)Fin du test de lecture du fichier .SOL...");
  }
}
