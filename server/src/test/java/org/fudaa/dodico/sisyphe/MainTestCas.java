/*
 * @file         MainTestCas.java
 * @creation     1999-07-09
 * @modification $Date: 2006-09-19 14:45:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sisyphe;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheCAS;

import org.fudaa.dodico.sisyphe.DParametresSisyphe;
/**
 * @version      $Revision: 1.1 $ $Date: 2006-09-19 14:45:47 $ by $Author: deniger $
 * @author       Mickael Rubens 
 */
public final class MainTestCas {
  private MainTestCas() {}
  /**
   * @param _args
   */
  public static void main(final String[] _args) {
    SParametresSisypheCAS params= new SParametresSisypheCAS();
    params=
      DParametresSisyphe.litParametresCAS(
        "/home/users/rubens/sisyphe_fic/cas/ca1_3");
    DParametresSisyphe.ecritParametresCAS("test.EcritCas", params);
    params= DParametresSisyphe.litParametresCAS("test.EcritCas");
    DParametresSisyphe.ecritParametresCAS("ca1_3", params);
  }
}
