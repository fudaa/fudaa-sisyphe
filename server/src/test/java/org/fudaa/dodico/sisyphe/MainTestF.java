/*
 * @file         MainTestF.java
 * @creation     1999-07-09
 * @modification $Date: 2006-10-19 14:12:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sisyphe;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheF;

import org.fudaa.dodico.sisyphe.DParametresSisyphe;
/**
 * @version      $Revision: 1.2 $ $Date: 2006-10-19 14:12:28 $ by $Author: deniger $
 * @author       Mickael Rubens 
 */
public final class MainTestF {
  
  private MainTestF(){}
  /**
   * @param args
   */
  public static void main(final String[] args) {
    System.out.println("1)Test de lecture/ecriture du fichier .J....");
    SParametresSisypheF params= new SParametresSisypheF();
    params=
      DParametresSisyphe.litParametresF(
        "/home/users/rubens/sisyphe_fic/cas/ca1_3");
    System.out.println("2)Affichage du contenu du fichier...");
    System.out.println(params.code);
    System.out.println("3) copie du fichier dans test.ecritF");
    DParametresSisyphe.ecritParametresF("ca1_3", params);
    System.out.println("3)Fin du test de lecture/Ecriture du fichier .J...");
  }
}
