/*
 * @file         MainTestPrs.java
 * @creation     
 * @modification $Date: 2006-09-19 14:45:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sisyphe;

import org.fudaa.dodico.sisyphe.DParametresSisyphe;

/**
 * @version      $Revision: 1.1 $ $Date: 2006-09-19 14:45:46 $ by $Author: deniger $
 * @author       Mickael Rubens 
 */
public final class MainTestPrs {
  
 private MainTestPrs(){}
  /**
   * @param _args
   */
  public static void main(final String[] _args) {
    System.out.println("1)Test d'ecriture du fichier .PRS....");
    DParametresSisyphe.ecritParametresPRS(
      "ca1_3",
      "/home/users/rubens/sisyphe_fic/cas/ca1_3",
      1100,
      4000,
      1000);
    System.out.println("3)Fin du test d'ecriture du fichier .PRS...");
  }
}
