/**
 * @file         DParametresSisyphe.java
 * @creation     1999-07-09
 * @modification $Date: 2007-04-30 14:21:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sisyphe;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.corba.sisyphe.IParametresSisyphe;
import org.fudaa.dodico.corba.sisyphe.IParametresSisypheOperations;
import org.fudaa.dodico.corba.sisyphe.SParametresCalculCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresConsPhysCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresDureeCalcCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresESFichiersCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresESGeneralitesCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresESGraphListCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresElmtsBlocSisypheGEO;
import org.fudaa.dodico.corba.sisyphe.SParametresEnvironnementCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresGeneralCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresLigneSisypheCOND;
import org.fudaa.dodico.corba.sisyphe.SParametresLogicielDessinCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresNoeudsBlocSisypheGEO;
import org.fudaa.dodico.corba.sisyphe.SParametresNomCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresParamNumCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheCOND;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheF;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheGEO;
import org.fudaa.dodico.corba.sisyphe.SParametresSisyphePRE;
import org.fudaa.dodico.corba.sisyphe.SParametresSisypheREF;
import org.fudaa.dodico.corba.sisyphe.SParametresSolveurCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresStdCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresTransSolidCAS;
import org.fudaa.dodico.corba.sisyphe.SParametresVarsBlocSisypheGEO;
import org.fudaa.dodico.corba.sisyphe.SResultatsSisypheSOL;
import org.fudaa.dodico.fortran.FortranBinaryInputStream;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * Les parametres Sysiphe.
 * 
 * @version $Revision: 1.11 $ $Date: 2007-04-30 14:21:38 $ by $Author: deniger $
 * @author Mickael Rubens
 */
public class DParametresSisyphe extends DParametres implements IParametresSisyphe, IParametresSisypheOperations {
  private SParametresSisypheCOND paramsCOND_;
  private SParametresSisypheCAS paramsCAS_;
  private SParametresSisypheGEO paramsGEO_;
  private SParametresSisypheF paramsF_;
  private SParametresSisypheREF paramsREF_;
  private SParametresSisyphePRE paramsPRE_;

  public DParametresSisyphe() {
    super();
  }

  public final Object clone() throws CloneNotSupportedException {
    return new DParametresSisyphe();
  }

  public String toString() {
    final String s = "DParametresSisyphe";
    return s;
  }

  /**
   * @return Fonctions d'Ecriture des donnees dans les fichiers.Les fichiers .COND.
   */
  public SParametresSisypheCOND parametresSisypheCOND() {
    return paramsCOND_;
  }

  public void parametresSisypheCOND(final SParametresSisypheCOND _paramsCOND) {
    paramsCOND_ = _paramsCOND;
  }

  /**
   * Les fichiers CAS.
   * 
   * @return paramsCAS_
   */
  public SParametresSisypheCAS parametresSisypheCAS() {
    return paramsCAS_;
  }

  /**
   * Les fichiers CAS.
   * 
   * @param _paramsCAS
   */
  public void parametresSisypheCAS(final SParametresSisypheCAS _paramsCAS) {
    paramsCAS_ = _paramsCAS;
  }

  /**
   * @return Les fichiers GEO.
   */
  public SParametresSisypheGEO parametresSisypheGEO() {
    return paramsGEO_;
  }

  /**
   * @param _paramsGEO Les fichiers GEO.
   */
  public void parametresSisypheGEO(final SParametresSisypheGEO _paramsGEO) {
    paramsGEO_ = _paramsGEO;
  }

  /**
   * @return Les fichiers F.
   */
  public SParametresSisypheF parametresSisypheF() {
    return paramsF_;
  }

  /**
   * @param _paramsF Les fichiers F.
   */
  public void parametresSisypheF(final SParametresSisypheF _paramsF) {
    paramsF_ = _paramsF;
  }

  /**
   * @return Les fichiers REF.
   */
  public SParametresSisypheREF parametresSisypheREF() {
    return paramsREF_;
  }

  /**
   * Les fichiers REF.
   * 
   * @param _paramsREF
   */
  public void parametresSisypheREF(final SParametresSisypheREF _paramsREF) {
    paramsREF_ = _paramsREF;
  }

  /**
   * @return Les fichiers PRE.
   */
  public SParametresSisyphePRE parametresSisyphePRE() {
    return paramsPRE_;
  }

  /**
   * Les fichiers PRE.
   * 
   * @param _paramsPRE
   */
  public void parametresSisyphePRE(final SParametresSisyphePRE _paramsPRE) {
    paramsPRE_ = _paramsPRE;
  }

  /**
   * retourne la valeur TabEntier de DParametresSisyphe class.
   * 
   * @param _chaine
   * @return La valeur TabEntier
   */
  public static int[] getTabEntier(final String _chaine) {
    final int[] tab = new int[3];
    int i1;
    int i2;
    _chaine.trim();
    i1 = _chaine.indexOf(';');
    i2 = _chaine.lastIndexOf(';');
    tab[0] = (new Integer(_chaine.substring(0, i1))).intValue();
    tab[1] = (new Integer(_chaine.substring(i1 + 1, i2))).intValue();
    tab[2] = (new Integer(_chaine.substring(i2 + 1, _chaine.length()))).intValue();
    return tab;
  }

  /**
   * Methode d'ecriture des donnees du fichier cond.
   * 
   * @param _fichier
   * @param _params
   */
  public static void ecritParametresCOND(final File _fichier, final SParametresSisypheCOND _params) {
    ecritParametresCOND(_fichier.toString(), _params);
  }

  public static void ecritParametresCOND(final String _fichier, final SParametresSisypheCOND _params) {
    try {
      final FortranWriter fcond = new FortranWriter(new FileWriter(_fichier));
      System.out.println("Ecriture du fichier " + _fichier + "!");
      for (int ligne = 0; ligne < _params.nbLignes; ligne++) {
        fcond.intField(1, _params.ligne[ligne].X1);
        fcond.intField(2, _params.ligne[ligne].X2);
        fcond.intField(3, _params.ligne[ligne].X3);
        fcond.doubleField(4, _params.ligne[ligne].X4);
        fcond.doubleField(5, _params.ligne[ligne].X5);
        fcond.doubleField(6, _params.ligne[ligne].X6);
        fcond.doubleField(7, _params.ligne[ligne].X7);
        fcond.intField(8, _params.ligne[ligne].LIEBOR);
        fcond.doubleField(9, _params.ligne[ligne].EBOR);
        fcond.doubleField(10, _params.ligne[ligne].X10);
        fcond.doubleField(11, _params.ligne[ligne].X11);
        fcond.intField(12, _params.ligne[ligne].N);
        fcond.intField(13, _params.ligne[ligne].K);
        fcond.writeFields();
      }
      fcond.close();
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
  }

  /**
   * Methode de lecture des donnees du fichier cond.
   */
  public static SParametresSisypheCOND litParametresCOND(final File fichier) {
    return litParametresCOND(fichier.toString());
  }

  public static SParametresSisypheCOND litParametresCOND(final String fichier) {
    final SParametresSisypheCOND params = new SParametresSisypheCOND();
    params.ligne = new SParametresLigneSisypheCOND[0];
    params.nbLignes = 0;
    final Vector pLigne = new Vector(375, 375);
    // System.out.println("Lecture du fichier "+fichier+".cond!") ;
    System.out.println("Lecture du fichier " + fichier);
    try {
      // FortranReader fcond = new FortranReader(new FileReader(fichier+".cond")) ;
      final FortranReader fcond = new FortranReader(new FileReader(fichier));
      boolean NOEOF = true;
      if (fcond.ready()) {
        while (NOEOF) {
          int i = 0;
          try {
            fcond.readFields();
          } catch (final EOFException eof) {
            NOEOF = false;
            continue;
          }
          final SParametresLigneSisypheCOND ligne = new SParametresLigneSisypheCOND();
          ligne.X1 = fcond.intField(i++);
          ligne.X2 = fcond.intField(i++);
          ligne.X3 = fcond.intField(i++);
          ligne.X4 = fcond.doubleField(i++);
          ligne.X5 = fcond.doubleField(i++);
          ligne.X6 = fcond.doubleField(i++);
          ligne.X7 = fcond.doubleField(i++);
          ligne.LIEBOR = fcond.intField(i++);
          ligne.EBOR = fcond.doubleField(i++);
          ligne.X10 = fcond.doubleField(i++);
          ligne.X11 = fcond.doubleField(i++);
          ligne.N = fcond.intField(i++);
          ligne.K = fcond.intField(i++);
          pLigne.add(ligne);
          params.nbLignes++;
        }
        // params.ligne = new SParametresLigneSisypheCOND[params.nbLignes] ;
        System.out.println(params.nbLignes);
        params.ligne = (SParametresLigneSisypheCOND[]) pLigne.toArray(new SParametresLigneSisypheCOND[params.nbLignes]);
        fcond.close();
      }
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
    return params;
  }

  /**
   * ....
   * 
   * @param fichier
   * @param params
   */
  public static void ecritParametresCAS(final File fichier, final SParametresSisypheCAS params) {
    ecritParametresCAS(fichier.toString(), params);
  }

  /**
   * ....
   * 
   * @param fichier
   * @param params
   */
  public static void ecritParametresCAS(final String fichier, final SParametresSisypheCAS params) {
    try {
      final FortranWriter fcas = new FortranWriter(new FileWriter(fichier));
      // System.out.println("Ecriture du fichier "+fichier) ;
      System.out.println("***E/S, Fichiers: ");
      System.out.println("NOMS");
      final String eq = " = ";
      if (params.esFic.noms.nomfon.length() > 0) {
        fcas.writeln(MotsClefs.nomfon + eq + params.esFic.noms.nomfon);
      }
      if (params.esFic.noms.nompre.length() > 0) {
        fcas.writeln(MotsClefs.nompre + eq + params.esFic.noms.nompre);
      }
      if (params.esFic.noms.nomprs.length() > 0) {
        fcas.writeln(MotsClefs.nomprs + eq + params.esFic.noms.nomprs);
      }
      if (params.esFic.noms.nomref.length() > 0) {
        fcas.writeln(MotsClefs.nomref + eq + params.esFic.noms.nomref);
      }
      if (params.esFic.noms.nomgeo.length() > 0) {
        fcas.writeln(MotsClefs.nomgeo + eq + params.esFic.noms.nomgeo);
      }
      if (params.esFic.noms.nomf.length() > 0) {
        fcas.writeln(MotsClefs.nomf + eq + params.esFic.noms.nomf);
      }
      if (params.esFic.noms.nomcas.length() > 0) {
        fcas.writeln(MotsClefs.nomcas + eq + params.esFic.noms.nomcas);
      }
      if (params.esFic.noms.nomcond.length() > 0) {
        fcas.writeln(MotsClefs.nomcond + eq + params.esFic.noms.nomcond);
      }
      if (params.esFic.noms.nomsol.length() > 0) {
        fcas.writeln(MotsClefs.nomsol + eq + params.esFic.noms.nomsol);
      }
      System.out.println("STANDARD");
      fcas.writeln(MotsClefs.bingeo + eq + params.esFic.std.bingeo);
      fcas.writeln(MotsClefs.binpre + eq + params.esFic.std.binpre);
      fcas.writeln(MotsClefs.binprs + eq + params.esFic.std.binprs);
      fcas.writeln(MotsClefs.binres + eq + params.esFic.std.binres);
      fcas.writeln(MotsClefs.binref + eq + params.esFic.std.binref);
      System.out.println("***E/S,Generalites");
      System.out.println("CALCUL");
      if (params.esGen.calcul.titca.length() > 0) {
        fcas.writeln(MotsClefs.titca + eq + params.esGen.calcul.titca);
      }
      fcas.writeln(MotsClefs.numver + eq + params.esGen.calcul.numver);
      fcas.writeln(MotsClefs.bibli + eq + params.esGen.calcul.bibli);
      fcas.writeln(MotsClefs.perma + eq + new Boolean(params.esGen.calcul.perma).toString().toUpperCase());
      fcas.writeln(MotsClefs.valid + eq + new Boolean(params.esGen.calcul.valid).toString().toUpperCase());
      fcas.writeln(MotsClefs.pdtvar + eq + new Boolean(params.esGen.calcul.pdtvar).toString().toUpperCase());
      System.out.println("ENVIRONNEMENT");
      if (params.esGen.env.ucray.length() > 0) {
        fcas.writeln(MotsClefs.ucray + eq + params.esGen.env.ucray);
      }
      fcas.writeln(MotsClefs.tmcray + eq + params.esGen.env.tmcray);
      if (params.esGen.env.pwdcray.length() > 0) {
        fcas.writeln(MotsClefs.pwdcray + eq + params.esGen.env.pwdcray);
      }
      fcas.writeln(MotsClefs.memcray + eq + params.esGen.env.memcray);
      System.out.println("TYPE DU LOGICIEL DE DESSIN");
      fcas.writeln(MotsClefs.logdes + eq + params.esGen.logiDes.logdes);
      fcas.writeln(MotsClefs.logpre + eq + params.esGen.logiDes.logpre);
      System.out.println("HYDRODYNAMIQUE");
      fcas.writeln(MotsClefs.hydro + eq + params.esGen.hydro);
      System.out.println("TYPE DU MAILLEUR");
      fcas.writeln(MotsClefs.mailleur + eq + params.esGen.mailleur);
      System.out.println("***E/S,Graphiques et Listing");
      fcas.writeln(MotsClefs.sortis + eq + params.esGrLst.sortis);
      if (params.esGrLst.varim.length() > 0) {
        fcas.writeln(MotsClefs.varim + eq + params.esGrLst.varim);
      }
      fcas.writeln(MotsClefs.bilma + eq + new Boolean(params.esGrLst.bilma).toString().toUpperCase());
      fcas.writeln(MotsClefs.leopr + eq + params.esGrLst.leopr);
      fcas.writeln(MotsClefs.lispr + eq + params.esGrLst.lispr);
      System.out.println("***Parametres Numeriques");
      System.out.println("GENERAL");
      fcas.writeln(MotsClefs.bandec + eq + new Boolean(params.paramNum.general.bandec).toString().toUpperCase());
      fcas.writeln(MotsClefs.calmeth + eq + params.paramNum.general.calmeth);
      fcas.writeln(MotsClefs.n1 + eq + params.paramNum.general.n1);
      fcas.writeln(MotsClefs.optban + eq + params.paramNum.general.optban);
      fcas.writeln(MotsClefs.lvect + eq + params.paramNum.general.lvect);
      fcas.writeln(MotsClefs.zero + eq + params.paramNum.general.zero);
      fcas.writeln(MotsClefs.teta + eq + params.paramNum.general.teta);
      fcas.writeln(MotsClefs.beta + eq + params.paramNum.general.beta);
      System.out.println("DUREE DU CALCUL");
      fcas.writeln(MotsClefs.delt + eq + params.paramNum.dureeCalc.delt);
      fcas.writeln(MotsClefs.orghydro + eq + params.paramNum.dureeCalc.orghydro);
      fcas.writeln(MotsClefs.pmaree + eq + params.paramNum.dureeCalc.pmaree);
      fcas.writeln(MotsClefs.npas + eq + params.paramNum.dureeCalc.npas);
      fcas.writeln(MotsClefs.nmarees + eq + params.paramNum.dureeCalc.nmarees);
      fcas.writeln(MotsClefs.nsous + eq + params.paramNum.dureeCalc.nsous);
      System.out.println("***Solveur");
      fcas.writeln(MotsClefs.lump + eq + new Boolean(params.solveur.lump).toString().toUpperCase());
      fcas.writeln(MotsClefs.solveur + eq + params.solveur.solveur);
      fcas.writeln(MotsClefs.optsolv + eq + params.solveur.optsolv);
      fcas.writeln(MotsClefs.precon + eq + params.solveur.precon);
      fcas.writeln(MotsClefs.maxit + eq + params.solveur.maxit);
      fcas.writeln(MotsClefs.psolveur + eq + params.solveur.psolveur);
      System.out.println("***Vitesse Celerite Hauteur");
      fcas.writeln(MotsClefs.hmin + eq + params.hmin);
      System.out.println("***Equations");
      System.out.println("FROTTEMENT ET LISSAGE");
      fcas.writeln(MotsClefs.sfon + eq + params.sfon);
      System.out.println("***Transport Solide");
      fcas.writeln(MotsClefs.icf + eq + params.transSol.icf);
      fcas.writeln(MotsClefs.rc + eq + params.transSol.rc);
      fcas.writeln(MotsClefs.dm + eq + params.transSol.dm);
      fcas.writeln(MotsClefs.ac + eq + params.transSol.ac);
      fcas.writeln(MotsClefs.xkv + eq + params.transSol.xkv);
      fcas.writeln(MotsClefs.optfond + eq + params.transSol.optfond);
      System.out.println("***Constantes Physiques");
      fcas.writeln(MotsClefs.xmve + eq + params.consPhys.xmve);
      fcas.writeln(MotsClefs.xmvs + eq + params.consPhys.xmvs);
      fcas.writeln(MotsClefs.grav + eq + params.consPhys.grav);
      fcas.writeln(MotsClefs.vce + eq + params.consPhys.vce);
      fcas.close();
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
  }

  public static String emptyString() {
    return "";
  }

  /**
   * Les valeurs par defaut du dictionnaire n'etant pas connues, nous allons arbitrairement mettre des valeurs.
   * 
   * @return param
   */
  public static SParametresSisypheCAS initDefaut() {
    final SParametresSisypheCAS params = new SParametresSisypheCAS();
    params.esFic = new SParametresESFichiersCAS();
    params.esFic.noms = new SParametresNomCAS();
    params.esFic.std = new SParametresStdCAS();
    params.esGen = new SParametresESGeneralitesCAS();
    params.esGen.calcul = new SParametresCalculCAS();
    params.esGen.env = new SParametresEnvironnementCAS();
    params.esGen.logiDes = new SParametresLogicielDessinCAS();
    params.esGrLst = new SParametresESGraphListCAS();
    params.paramNum = new SParametresParamNumCAS();
    params.paramNum.general = new SParametresGeneralCAS();
    params.paramNum.dureeCalc = new SParametresDureeCalcCAS();
    params.solveur = new SParametresSolveurCAS();
    params.transSol = new SParametresTransSolidCAS();
    params.consPhys = new SParametresConsPhysCAS();
    try {
      // E/S, Fichiers: NOMS
      params.esFic.noms.nomfon = emptyString();
      params.esFic.noms.nompre = emptyString();
      params.esFic.noms.nomprs = emptyString();
      params.esFic.noms.nomref = emptyString();
      params.esFic.noms.nomgeo = emptyString();
      params.esFic.noms.nomf = emptyString();
      params.esFic.noms.nomcas = emptyString();
      params.esFic.noms.nomcond = emptyString();
      params.esFic.noms.nomsol = emptyString();
      // STANDARD
      final String STD = "STD";
      params.esFic.std.bingeo = STD;
      params.esFic.std.binpre = STD;
      params.esFic.std.binprs = STD;
      params.esFic.std.binres = STD;
      params.esFic.std.binref = STD;
      // E/S,Generalites CALCUL
      params.esGen.calcul.titca = emptyString();
      params.esGen.calcul.numver = "V1P1,V4P0,V4P0,V4P0,V4P0";
      params.esGen.calcul.bibli = "SISYPHE,UTIL,DAMO,BIEF,HP";
      params.esGen.calcul.perma = false;
      params.esGen.calcul.valid = false;
      params.esGen.calcul.pdtvar = false;
      // ENVIRONNEMENT
      params.esGen.env.ucray = emptyString();
      params.esGen.env.tmcray = "10";
      params.esGen.env.pwdcray = emptyString();
      params.esGen.env.memcray = "1500000W";
      // TYPE DU LOGICIEL DE DESSIN
      params.esGen.logiDes.logdes = 1;
      params.esGen.logiDes.logpre = 3;
      // HYDRODYNAMIQUE
      params.esGen.hydro = 1;
      // TYPE DU MAILLEUR
      params.esGen.mailleur = 3;
      // E/S,Graphiques et Listing
      params.esGrLst.sortis = "U,V,S,E,H,B";
      params.esGrLst.varim = emptyString();
      params.esGrLst.bilma = false;
      params.esGrLst.leopr = 1;
      params.esGrLst.lispr = 1;
      // Parametres Numeriques GENERAL
      params.paramNum.general.bandec = true;
      params.paramNum.general.calmeth = 5;
      params.paramNum.general.n1 = 1;
      params.paramNum.general.optban = 1;
      params.paramNum.general.lvect = 1;
      params.paramNum.general.zero = 1.E-20;
      params.paramNum.general.teta = 0.;
      params.paramNum.general.beta = 0.;
      // DUREE DU CALCUL
      params.paramNum.dureeCalc.delt = 1.;
      params.paramNum.dureeCalc.orghydro = 0.;
      params.paramNum.dureeCalc.pmaree = 44640.;
      params.paramNum.dureeCalc.npas = 1;
      params.paramNum.dureeCalc.nmarees = 1;
      params.paramNum.dureeCalc.nsous = 1;
      // Solveur
      params.solveur.lump = true;
      params.solveur.solveur = 3;
      params.solveur.optsolv = 2;
      params.solveur.precon = 2;
      params.solveur.maxit = 60;
      params.solveur.psolveur = 1.E-4;
      // Vitesse Celerite Hauteur
      params.hmin = 1.E-3;
      // Equations FROTTEMENT ET LISSAGE
      params.sfon = 50.;
      // Transport Solide
      params.transSol.icf = 1;
      params.transSol.rc = 1.;
      params.transSol.dm = 0.001;
      params.transSol.ac = 0.047;
      params.transSol.xkv = 1.;
      params.transSol.optfond = 0;
      // Constantes Physiques
      params.consPhys.xmve = 1000.;
      params.consPhys.xmvs = 2665.;
      params.consPhys.grav = 9.81;
      params.consPhys.vce = 1.E-6;
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
    return params;
  }

  /**
   * Va relever les mots clefs utilises dans le fichier cas. Va les ranger (avec la valeur qui leur est associee) dans
   * une table de Hash
   */
  public static Hashtable litMotsClefs(final String fichier) {
    final Hashtable tmc = new Hashtable();
    try {
      // FortranReader fcas = new FortranReader(new FileReader(fichier+".cas")) ;
      final FortranReader fcas = new FortranReader(new FileReader(fichier));
      boolean NOEOF = true;
      String ligne = emptyString();
      String MotClef = emptyString();
      String Affectation = emptyString();
      int iAffectation;
      if (fcas.ready()) {
        // Lit tant que pas en fin de fichier
        while (NOEOF) {
          try {
            // Passe les commentaires
            do {
              fcas.readFields();
              ligne = fcas.getLine();
              ligne.trim();
            } while ((ligne.length() < 2) || (ligne.charAt(0) == '/'));
            // Recherche le ssymbol d'affectation ( '=' ou ':' )
            if (ligne.charAt(0) != '&') {
              iAffectation = ligne.indexOf("=");
              if (iAffectation == -1) {
                iAffectation = ligne.indexOf(":");
              }
              // Decompose la ligne en un MotClef et sa Valeur (Affectation)
              MotClef = ligne.substring(0, iAffectation - 1);
              MotClef = MotClef.trim();
              Affectation = ligne.substring(iAffectation + 1, ligne.length());
              Affectation = Affectation.trim();
              // Remplissage de la table
              tmc.put(MotClef, Affectation);
            } else if ((ligne == "&FIN") || (ligne == "&STO")) {
              NOEOF = false;
            }
          } catch (final EOFException eof) {
            NOEOF = false;
            continue;
          }
        }
      }
      fcas.close();
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
    return tmc;
  }

  public static SParametresSisypheCAS litParametresCAS(final File fichier) {
    return litParametresCAS(fichier.toString());
  }

  public static SParametresSisypheCAS litParametresCAS(final String fichier) {
    SParametresSisypheCAS params;
    Hashtable tableMotsClefs = new Hashtable();
    // initialisation par defaut des parametres
    params = initDefaut();
    tableMotsClefs = litMotsClefs(fichier);
    try {
      String chaine;
      System.out.println("***E/S, Fichiers: ");
      System.out.println("NOMS");
      chaine = (String) tableMotsClefs.get(MotsClefs.nomfon);
      if (chaine != null) {
        params.esFic.noms.nomfon = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.nompre);
      if (chaine != null) {
        params.esFic.noms.nompre = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.nomprs);
      if (chaine != null) {
        params.esFic.noms.nomprs = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.nomref);
      if (chaine != null) {
        params.esFic.noms.nomref = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.nomgeo);
      if (chaine != null) {
        params.esFic.noms.nomgeo = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.nomf);
      if (chaine != null) {
        params.esFic.noms.nomf = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.nomcas);
      if (chaine != null) {
        params.esFic.noms.nomcas = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.nomcond);
      if (chaine != null) {
        params.esFic.noms.nomcond = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.nomsol);
      if (chaine != null) {
        params.esFic.noms.nomsol = chaine;
      }
      System.out.println("STANDARD");
      chaine = (String) tableMotsClefs.get(MotsClefs.bingeo);
      if (chaine != null) {
        params.esFic.std.bingeo = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.binpre);
      if (chaine != null) {
        params.esFic.std.binpre = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.bingeo);
      if (chaine != null) {
        params.esFic.std.binres = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.binres);
      if (chaine != null) {
        params.esFic.std.binres = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.binref);
      if (chaine != null) {
        params.esFic.std.binref = chaine;
      }
      System.out.println("***E/S,Generalites");
      System.out.println("CALCUL");
      chaine = (String) tableMotsClefs.get(MotsClefs.titca);
      if (chaine != null) {
        params.esGen.calcul.titca = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.numver);
      if (chaine != null) {
        params.esGen.calcul.numver = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.bibli);
      if (chaine != null) {
        params.esGen.calcul.bibli = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.perma);
      if (chaine != null) {
        params.esGen.calcul.perma = MyBool.boolTest(chaine);
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.valid);
      if (chaine != null) {
        params.esGen.calcul.valid = MyBool.boolTest(chaine);
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.pdtvar);
      if (chaine != null) {
        params.esGen.calcul.pdtvar = MyBool.boolTest(chaine);
      }
      System.out.println("ENVIRONNEMENT");
      chaine = (String) tableMotsClefs.get(MotsClefs.ucray);
      if (chaine != null) {
        params.esGen.env.ucray = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.tmcray);
      if (chaine != null) {
        params.esGen.env.tmcray = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.pwdcray);
      if (chaine != null) {
        params.esGen.env.pwdcray = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.memcray);
      if (chaine != null) {
        params.esGen.env.memcray = chaine;
      }
      System.out.println("TYPE DU LOGICIEL DE DESSIN");
      chaine = (String) tableMotsClefs.get(MotsClefs.logdes);
      if (chaine != null) {
        params.esGen.logiDes.logdes = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.logpre);
      if (chaine != null) {
        params.esGen.logiDes.logpre = (new Integer(chaine)).intValue();
      }
      System.out.println("HYDRODYNAMIQUE");
      chaine = (String) tableMotsClefs.get(MotsClefs.hydro);
      if (chaine != null) {
        params.esGen.hydro = (new Integer(chaine)).intValue();
      }
      System.out.println("TYPE DU MAILLEUR");
      chaine = (String) tableMotsClefs.get(MotsClefs.mailleur);
      if (chaine != null) {
        params.esGen.mailleur = (new Integer(chaine)).intValue();
      }
      System.out.println("***E/S,Graphiques et Listing");
      chaine = (String) tableMotsClefs.get(MotsClefs.sortis);
      if (chaine != null) {
        params.esGrLst.sortis = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.varim);
      if (chaine != null) {
        params.esGrLst.varim = chaine;
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.bilma);
      if (chaine != null) {
        params.esGrLst.bilma = MyBool.boolTest(chaine);
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.leopr);
      if (chaine != null) {
        params.esGrLst.leopr = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.lispr);
      if (chaine != null) {
        params.esGrLst.lispr = (new Integer(chaine)).intValue();
      }
      System.out.println("***Parametres Numeriques");
      System.out.println("GENERAL");
      chaine = (String) tableMotsClefs.get(MotsClefs.bandec);
      if (chaine != null) {
        params.paramNum.general.bandec = MyBool.boolTest(chaine);
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.calmeth);
      if (chaine != null) {
        params.paramNum.general.calmeth = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.n1);
      if (chaine != null) {
        params.paramNum.general.n1 = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.optban);
      if (chaine != null) {
        params.paramNum.general.optban = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.lvect);
      if (chaine != null) {
        params.paramNum.general.lvect = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.zero);
      if (chaine != null) {
        params.paramNum.general.zero = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.teta);
      if (chaine != null) {
        params.paramNum.general.teta = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.beta);
      if (chaine != null) {
        params.paramNum.general.beta = (new Double(chaine)).doubleValue();
      }
      System.out.println("DUREE DU CALCUL");
      chaine = (String) tableMotsClefs.get(MotsClefs.delt);
      if (chaine != null) {
        params.paramNum.dureeCalc.delt = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.orghydro);
      if (chaine != null) {
        params.paramNum.dureeCalc.orghydro = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.pmaree);
      if (chaine != null) {
        params.paramNum.dureeCalc.pmaree = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.npas);
      if (chaine != null) {
        params.paramNum.dureeCalc.npas = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.nmarees);
      if (chaine != null) {
        params.paramNum.dureeCalc.nmarees = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.nsous);
      if (chaine != null) {
        params.paramNum.dureeCalc.nsous = (new Integer(chaine)).intValue();
      }
      System.out.println("***Solveur");
      chaine = (String) tableMotsClefs.get(MotsClefs.lump);
      if (chaine != null) {
        params.solveur.lump = MyBool.boolTest(chaine);
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.solveur);
      if (chaine != null) {
        params.solveur.solveur = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.optsolv);
      if (chaine != null) {
        params.solveur.optsolv = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.precon);
      if (chaine != null) {
        params.solveur.precon = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.maxit);
      if (chaine != null) {
        params.solveur.maxit = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.psolveur);
      if (chaine != null) {
        params.solveur.psolveur = (new Double(chaine)).doubleValue();
      }
      System.out.println("***Vitesse Celerite Hauteur");
      chaine = (String) tableMotsClefs.get(MotsClefs.hmin);
      if (chaine != null) {
        params.hmin = (new Double(chaine)).doubleValue();
      }
      System.out.println("***Equations");
      System.out.println("FROTTEMENT ET LISSAGE");
      chaine = (String) tableMotsClefs.get(MotsClefs.sfon);
      if (chaine != null) {
        params.sfon = (new Double(chaine)).doubleValue();
      }
      System.out.println("***Transport Solide");
      chaine = (String) tableMotsClefs.get(MotsClefs.icf);
      if (chaine != null) {
        params.transSol.icf = (new Integer(chaine)).intValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.rc);
      if (chaine != null) {
        params.transSol.rc = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.dm);
      if (chaine != null) {
        params.transSol.dm = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.ac);
      if (chaine != null) {
        params.transSol.ac = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.xkv);
      if (chaine != null) {
        params.transSol.xkv = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.optfond);
      if (chaine != null) {
        params.transSol.optfond = (new Integer(chaine)).intValue();
      }
      System.out.println("***Constantes Physiques");
      chaine = (String) tableMotsClefs.get(MotsClefs.xmve);
      if (chaine != null) {
        params.consPhys.xmve = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.xmvs);
      if (chaine != null) {
        params.consPhys.xmvs = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.grav);
      if (chaine != null) {
        params.consPhys.grav = (new Double(chaine)).doubleValue();
      }
      chaine = (String) tableMotsClefs.get(MotsClefs.vce);
      if (chaine != null) {
        params.consPhys.vce = (new Double(chaine)).doubleValue();
      }
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
    return params;
  }

  /**
   * ....
   * 
   * @param fichier
   * @param params
   */
  public static void ecritParametresGEO(final File fichier, final SParametresSisypheGEO params) {
    // nbPasDeTemps =1, cf: litParametresGEO(File)
    ecritParametresGEO(fichier.toString(), params, 1);
  }

  /**
   * ....
   * 
   * @param fichier
   * @param params
   * @param nbPasDeTemps
   */
  public static void ecritParametresGEO(final String fichier, final SParametresSisypheGEO params, final int nbPasDeTemps) {
    try {
      final FortranWriter fgeo = new FortranWriter(new FileWriter(fichier));
      System.out.println("Ecriture du fichier " + fichier + "!");
      fgeo.stringField(0, params.titre);
      fgeo.writeFields(new int[] { 72 });
      fgeo.intField(0, params.NBV[0]);
      fgeo.intField(1, params.NBV[1]);
      fgeo.writeFields(new int[] { 5, 5 });
      for (int i = 0; i < params.NBV[0]; i++) {
        final int[] fmt = new int[] { 32 };
        fgeo.stringField(0, params.descVariables[i]);
        fgeo.writeFields(fmt);
      }
      fgeo.intField(0, params.NNT);
      fgeo.writeFields();
      for (int i = 0; i < params.NNT; i++) {
        final int[] fmt = new int[] { 5, 10, 10 };
        fgeo.intField(0, params.noeuds[i].numNoeud);
        fgeo.doubleField(1, params.noeuds[i].coordX);
        fgeo.doubleField(2, params.noeuds[i].coordY);
        fgeo.writeFields(fmt);
      }
      fgeo.intField(0, params.NELT);
      fgeo.intField(1, params.nbNoeudElt);
      fgeo.writeFields(new int[] { 5, 5 });
      for (int i = 0; i < params.NELT; i++) {
        fgeo.intField(0, params.elements[i].numElement);
        final int[] fmt = new int[1 + 5 + params.nbNoeudElt];
        for (int j = 0; j < 5 + params.nbNoeudElt; j++) {
          fmt[j] = 5;
          fgeo.intField(j + 1, params.elements[i].descElement[j]);
        }
        fmt[5 + params.nbNoeudElt] = 5;
        fgeo.writeFields(fmt);
      }
      int result = params.NNT / 16;
      int j = 0;
      for (int i = 0; i < result; i++) {
        final int[] fmt = new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
        for (int k = 0; k < 16; k++) {
          fgeo.intField(k, params.IPOB[j++]);
        }
        fgeo.writeFields(fmt);
      }
      int rest = params.NNT % 16;
      if (rest > 0) {
        final int[] fmtIPOB = new int[rest];
        for (int k = 0; k < rest; k++) {
          fgeo.intField(k, params.IPOB[j++]);
          fmtIPOB[k] = 5;
        }
        fgeo.writeFields(fmtIPOB);
      }
      result = params.NNT / 8;
      rest = params.NNT % 8;
      for (int i = 0; i < nbPasDeTemps; i++) {
        fgeo.doubleField(0, params.resultats[i].tempsBloc);
        fgeo.writeFields();
        for (int k = 0; k < params.NBV[0]; k++) {
          j = 0;
          for (int l = 0; l < result; l++) {
            final int[] fmt = new int[] { 10, 10, 10, 10, 10, 10, 10, 10 };
            for (int m = 0; m < 8; m++) {
              fgeo.doubleField(m, params.resultats[i].tabResultats[k][j++]);
            }
            fgeo.writeFields(fmt);
          }
          if (rest > 0) {
            final int[] fmtTemps = new int[rest];
            for (int m = 0; m < rest; m++) {
              fgeo.doubleField(m, params.resultats[i].tabResultats[k][j++]);
              fmtTemps[m] = 10;
            }
            fgeo.writeFields(fmtTemps);
          }
        }
      }
      fgeo.close();
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
  }

  public static SParametresSisypheGEO litParametresGEO(final File fichier) {
    /*
     * nbPasDeTemps est � 1. il fallait faire un choix puisqu'il n'y pas moyen de connaitre sa valeur. (Reference Doc
     * Technique Sisyphe Juil-Aout 99)
     */
    return litParametresGEO(fichier.toString(), 1);
  }

  public static SParametresSisypheGEO litParametresGEO(final String fichier, final int nbPasDeTemps) {
    final SParametresSisypheGEO params = new SParametresSisypheGEO();
    params.titre = emptyString();
    params.NBV = new int[0];
    params.descVariables = new String[0];
    params.noeuds = new SParametresNoeudsBlocSisypheGEO[0];
    params.elements = new SParametresElmtsBlocSisypheGEO[0];
    params.IPOB = new int[0];
    params.resultats = new SParametresVarsBlocSisypheGEO[0];
    try {
      final FortranReader fgeo = new FortranReader(new FileReader(fichier));
      if (fgeo.ready()) {
        System.out.println("Lecture du fichier " + fichier);
        fgeo.readFields(new int[] { 72 });
        params.titre = new String(fgeo.stringField(0));
        params.NBV = new int[2];
        fgeo.readFields(new int[] { 5, 5 });
        params.NBV[0] = fgeo.intField(0);
        params.NBV[1] = fgeo.intField(1);
        params.descVariables = new String[params.NBV[0]];
        for (int i = 0; i < params.NBV[0]; i++) {
          final int[] fmt = new int[] { 32 };
          fgeo.readFields(fmt);
          params.descVariables[i] = new String(fgeo.stringField(0));
        }
        fgeo.readFields();
        params.NNT = fgeo.intField(0);
        params.noeuds = new SParametresNoeudsBlocSisypheGEO[params.NNT];
        for (int i = 0; i < params.NNT; i++) {
          final int[] fmt = new int[] { 5, 10, 10 };
          fgeo.readFields(fmt);
          params.noeuds[i] = new SParametresNoeudsBlocSisypheGEO();
          params.noeuds[i].numNoeud = fgeo.intField(0);
          params.noeuds[i].coordX = fgeo.doubleField(1);
          params.noeuds[i].coordY = fgeo.doubleField(2);
        }
        fgeo.readFields(new int[] { 5, 5 });
        params.NELT = fgeo.intField(0);
        params.nbNoeudElt = fgeo.intField(1);
        params.elements = new SParametresElmtsBlocSisypheGEO[params.NELT];
        for (int i = 0; i < params.NELT; i++) {
          final int[] fmt = new int[1 + 5 + params.nbNoeudElt];
          for (int j = 0; j < 1 + 5 + params.nbNoeudElt; j++) {
            fmt[j] = 5;
          }
          fgeo.readFields(fmt);
          params.elements[i] = new SParametresElmtsBlocSisypheGEO();
          params.elements[i].numElement = fgeo.intField(0);
          params.elements[i].descElement = new int[5 + params.nbNoeudElt];
          for (int j = 0; j < 5 + params.nbNoeudElt; j++) {
            params.elements[i].descElement[j] = fgeo.intField(j + 1);
          }
        }
        int result = params.NNT / 16;
        int j = 0;
        params.IPOB = new int[params.NNT];
        for (int i = 0; i < result; i++) {
          final int[] fmt = new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
          fgeo.readFields(fmt);
          for (int k = 0; k < 16; k++) {
            params.IPOB[j++] = fgeo.intField(k);
          }
        }
        int rest = params.NNT % 16;
        if (rest > 0) {
          final int[] fmtIPOB = new int[rest];
          for (int k = 0; k < rest; k++) {
            fmtIPOB[k] = 5;
          }
          fgeo.readFields(fmtIPOB);
          for (int k = 0; k < rest; k++) {
            params.IPOB[j++] = fgeo.intField(k);
          }
        }
        params.resultats = new SParametresVarsBlocSisypheGEO[nbPasDeTemps];
        for (int i = 0; i < nbPasDeTemps; i++) {
          fgeo.readFields();
          params.resultats[i] = new SParametresVarsBlocSisypheGEO();
          params.resultats[i].tempsBloc = fgeo.doubleField(0);
          params.resultats[i].tabResultats = new double[params.NBV[0]][params.NNT];
          result = params.NNT / 8;
          rest = params.NNT % 8;
          for (int k = 0; k < params.NBV[0]; k++) {
            j = 0;
            for (int l = 0; l < result; l++) {
              final int[] fmt = new int[] { 10, 10, 10, 10, 10, 10, 10, 10 };
              fgeo.readFields(fmt);
              for (int m = 0; m < 8; m++) {
                params.resultats[i].tabResultats[k][j++] = fgeo.doubleField(m);
              }
            }
            if (rest > 0) {
              final int[] fmtTemps = new int[rest];
              for (int m = 0; m < rest; m++) {
                fmtTemps[m] = 10;
              }
              fgeo.readFields(fmtTemps);
              for (int m = 0; m < rest; m++) {
                params.resultats[i].tabResultats[k][j++] = fgeo.doubleField(m);
              }
            }
          }
        }
        fgeo.close();
      }
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
    return params;
  }

  /**
   * Methode d'ecriture des donnees du fichier f.
   * 
   * @param fichier
   * @param params
   */
  public static void ecritParametresF(final File fichier, final SParametresSisypheF params) {
    ecritParametresF(fichier.toString(), params);
  }

  /**
   * ....
   * 
   * @param fichier
   * @param params
   */
  public static void ecritParametresF(final String fichier, final SParametresSisypheF params) {
    try {
      final FileWriter fF = new FileWriter(fichier);
      System.out.println("Ecriture du fichier " + fichier + "!");
      fF.write(params.code);
      fF.close();
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
  }

  /**
   * Methode de lecture des donnees du fichier f.
   */
  public static SParametresSisypheF litParametresF(final File fichier) {
    return litParametresF(fichier.toString());
  }

  public static SParametresSisypheF litParametresF(final String fichier) {
    final SParametresSisypheF params = new SParametresSisypheF();
    params.code = emptyString();
    try {
      final FileInputStream fF = new FileInputStream(fichier);
      final byte[] code = new byte[(new Long((new File(fichier)).length())).intValue()];
      System.out.println("Lecture du fichier " + fichier);
      fF.read(code);
      params.code = new String(code);
      fF.close();
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
    return params;
  }

  /**
   * Methode d'ecriture des donnees du fichier .ref.
   * 
   * @param fichier
   * @param params
   */
  public static void ecritParametresREF(final File fichier, final SParametresSisypheREF params) {
    ecritParametresREF(fichier.toString(), params);
  }

  public static void ecritParametresREF(final String _fichier, final SParametresSisypheREF _params) {
    if (_fichier != null) {
      try {
        final FileWriter fref = new FileWriter(_fichier);
        System.out.println("Ecriture du fichier " + _fichier + "!");
        fref.write(_params.reference);
        fref.close();
      } catch (final Exception ex) {
        ex.printStackTrace();
        System.err.println("IT: " + ex);
      }
    };
  }

  /**
   * Methode de lecture des donnees du fichier ref.
   */
  public static SParametresSisypheREF litParametresREF(final File fichier) {
    return litParametresREF(fichier.toString());
  }

  public static SParametresSisypheREF litParametresREF(final String fichier) {
    final SParametresSisypheREF params = new SParametresSisypheREF();
    params.reference = emptyString();
    try {
      // FileInputStream fREF = new FileInputStream(fichier+".ref") ;
      final FileInputStream fREF = new FileInputStream(fichier);
      final byte[] reference = new byte[(new Long((new File(fichier)).length())).intValue()];
      System.out.println("Lecture du fichier " + fichier);
      fREF.read(reference);
      params.reference = new String(reference);
      fREF.close();
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
    return params;
  }

  /**
   * Methode d'ecriture des donnees du fichier PRE.
   * 
   * @param fichier
   * @param params
   */
  public static void ecritParametresPRE(final String fichier, final SParametresSisyphePRE params) {
  /*
   * try{ }catch(Exception ex){ ex.printStackTrace() ; System.err.println("IT: "+ ex) ; };
   */
  }

  /**
   * Methode de lecture des donnees du fichier PRE.
   */
  public static SParametresSisyphePRE litParametresPRE(final File fichier) {
    return litParametresPRE(fichier.toString());
  }

  public static SParametresSisyphePRE litParametresPRE(final String fichier) {
    final SParametresSisyphePRE params = new SParametresSisyphePRE();
    try {
      // FortranBinaryInputStream fpre=new FortranBinaryInputStream(new FileInputStream(fichier+".pre"), true);
      final FortranBinaryInputStream fpre = new FortranBinaryInputStream(new FileInputStream(fichier), true);
      params.titre = emptyString();
      params.NBV = new int[2];
      params.valeurs = new SParametresVarsBlocSisypheGEO[0];
      // Lecture du titre
      fpre.readRecord();
      params.titre = fpre.readCharacter(72);
      // Lecture du nombre de variables
      fpre.readRecord();
      params.NBV[0] = fpre.readInteger();
      params.NBV[1] = fpre.readInteger();
      params.descVariables = new String[params.NBV[0]];
      // Lecture de la description des NBV(1) variables
      for (int i = 0; i < params.NBV[0]; i++) {
        fpre.readRecord();
        params.descVariables[i] = fpre.readCharacter(32);
      }
      // Lecture de l'enregistrement contenant: 1000000000
      fpre.readRecord();
      for (int i = 0; i < 10; i++) {
        fpre.readInteger();
      }
      // Lecture du nombre de noeud et du nombre d'element
      fpre.readRecord();
      params.NELT = fpre.readInteger();
      params.NNT = fpre.readInteger();
      // on 'passe' 2 donnees
      fpre.readInteger();
      fpre.readInteger();
      /*
       * params.connect = new int [params.NELT][3]; //Lecture des connectivites fpre.readInteger(); for(int i=0; i<params.NELT;
       * i++) for(int j=0; j<3; j++) params.connect[i][j] = fpre.readInteger(); fpre.readInteger() ;
       */
      params.connect = new int[4][3];
      // Lecture des connectivites
      fpre.readRecord();
      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 3; j++) {
          params.connect[i][j] = fpre.readInteger();
        }
      }
      /*
       * // Lecture des point du bord params.IPOB = new double [params.NNT] fpre.readRecord(); for(int i=0; i<params.NNT;
       * i++) params.IPOB[i] = readInteger(); // Lecture des coordonnees X params.coorX = new double [];
       * fpre.readRecord() ; for(int i=0; i<;i++) params.coordX[] = fpre.readInteger(); // Lecture des coordonnees Y
       * params.coorY = new double []; fpre.readRecord() ; for(int i=0; i<;i++) params.coordX[] = fpre.readInteger();
       */
      fpre.close();
    } catch (final IOException ex) {
      // ex.printStackTrace() ;
      System.err.println("IT: " + ex);
    };
    return params;
  }

  /**
   * Le fichier PRS correspond aux donn�es du dernier pas de temps d'un calcul precedent Son Implementation peut donc
   * etre faite sans passer par une encapsulation IDL. Il suffit d'indiquer le fichier Solution de reference. Methode
   * d'ecriture des donnees du fichier .prs On va lire le fichier solution qui sert de reference, puis on ne prend les
   * donnees que de la derniere iteration
   * 
   * @param fichier
   * @param fichierSOL
   * @param NNT
   * @param nbPas
   * @param pList
   */
  public static void ecritParametresPRS(final String fichier, final String fichierSOL, final int NNT, final int nbPas,
      final int pList) {
    try {
      SResultatsSisypheSOL results = new SResultatsSisypheSOL();
      results = DResultatsSisyphe.litResultatsSOL(fichierSOL, NNT, nbPas, pList);
      final int[] fmt = new int[results.nbVal + 1];
      final FortranWriter fprs = new FortranWriter(new FileWriter(fichier + ".prs"));
      System.out.println("Ecriture du fichier " + fichier + ".prs!");
      for (int i = 0; i < NNT; i++) {
        fprs.intField(0, i + 1);
        fmt[0] = 5;
        for (int k = 0; k < results.nbVal; k++) {
          fprs.doubleField(k + 1, results.pdtSuivants[nbPas / pList - 1].pointsMaillage[i].resultVariables[k]);
          fmt[k + 1] = 12;
        }
        fprs.writeFields(fmt);
      }
      fprs.close();
    } catch (final Exception ex) {
      ex.printStackTrace();
      System.err.println("IT: " + ex);
    };
  }
}
