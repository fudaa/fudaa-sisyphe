/**
 * @file         DResultatsSisyphe.java
 * @creation     1999-07-09
 * @modification $Date: 2006-09-19 14:45:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sisyphe;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.corba.sisyphe.IResultatsSisyphe;
import org.fudaa.dodico.corba.sisyphe.IResultatsSisypheOperations;
import org.fudaa.dodico.corba.sisyphe.SPasDeTempsSuivantsSOL;
import org.fudaa.dodico.corba.sisyphe.SResultatsPointsMaillageSOL;
import org.fudaa.dodico.corba.sisyphe.SResultatsSisypheSOL;
import org.fudaa.dodico.fortran.FortranReader;

/**
 * Les resultats Sisyphe.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 14:45:50 $ by $Author: deniger $
 * @author Mickael Rubens
 */
public class DResultatsSisyphe extends DResultats implements IResultatsSisyphe, IResultatsSisypheOperations {
  private SResultatsSisypheSOL resultsSOL_;

  public DResultatsSisyphe() {
    super();
  }

  public final Object clone() throws CloneNotSupportedException {
    final DResultatsSisyphe r = new DResultatsSisyphe();
    return r;
  }

  public SResultatsSisypheSOL resultatsSisypheSOL() {
    return resultsSOL_;
  }

  public void resultatsSisypheSOL(final SResultatsSisypheSOL _resultsSOL) {
    resultsSOL_ = _resultsSOL;
  }

  public static SResultatsSisypheSOL litResultatsSOL(final File _fichier, final int _nnt, final int _nbPas,
      final int _pListing) {
    return litResultatsSOL(_fichier.toString(), _nnt, _nbPas, _pListing);
  }

  public static SResultatsSisypheSOL litResultatsSOL(final String _fichier, final int _nnt, final int _nbPas,
      final int _pListing) {
    final SResultatsSisypheSOL results = new SResultatsSisypheSOL();
    try {
      final FortranReader fsol = new FortranReader(new FileReader(_fichier));
      fsol.readFields(new int[] { 5 });
      results.indicePasDeTemps = fsol.intField(0);
      fsol.readFields(new int[] { 68, 12 });
      results.descPasDeTemps = new String(fsol.stringField(0));
      results.valTemps = fsol.doubleField(1);
      results.info = -1;
      fsol.readFields(new int[] { 5, 5 });
      results.nbVal = fsol.intField(1);
      results.pointsMaillage = new SResultatsPointsMaillageSOL[_nnt];
      for (int i = 0; i < _nnt; i++) {
        results.pointsMaillage[i] = new SResultatsPointsMaillageSOL();
        results.pointsMaillage[i].resultVariables = new double[results.nbVal];
        results.pointsMaillage[i].numPdt = 0;
        final int[] fmt = new int[results.nbVal + 1];
        fmt[0] = 5;
        for (int j = 1; j <= results.nbVal; j++) {
          fmt[j] = 12;
        }
        fsol.readFields(fmt);
        for (int j = 1; j <= results.nbVal; j++) {
          try {
            results.pointsMaillage[i].resultVariables[j - 1] = fsol.doubleField(j);
          } catch (final NumberFormatException ex) {
            System.err.println("IT: " + ex + " \non retourne 0");
            results.pointsMaillage[i].resultVariables[j - 1] = 0;
          }
        }
      }
      if (results.indicePasDeTemps == -999) {
        results.pdtSuivants = new SPasDeTempsSuivantsSOL[_nbPas / _pListing];
        for (int i = 0; i < (_nbPas / _pListing); i++) {
          fsol.readFields(new int[5]);
          // "-999" est reecrit a chaque pas de temps
          fsol.readFields(new int[] { 68, 12 });
          results.pdtSuivants[i] = new SPasDeTempsSuivantsSOL();
          results.pdtSuivants[i].descPasDeTemps = new String(fsol.stringField(0));
          results.pdtSuivants[i].valTemps = fsol.doubleField(1);
          results.pdtSuivants[i].pointsMaillage = new SResultatsPointsMaillageSOL[_nnt];
          for (int j = 0; j < _nnt; j++) {
            results.pdtSuivants[i].pointsMaillage[j] = new SResultatsPointsMaillageSOL();
            results.pdtSuivants[i].pointsMaillage[j].resultVariables = new double[results.nbVal];
            results.pdtSuivants[i].pointsMaillage[j].numPdt = i;
            final int[] fmt = new int[results.nbVal + 1];
            fmt[0] = 5;
            for (int k = 1; k <= results.nbVal; k++) {
              fmt[k] = 12;
            }
            fsol.readFields(fmt);
            for (int k = 1; k <= results.nbVal; k++) {
              try {
                results.pdtSuivants[i].pointsMaillage[j].resultVariables[k - 1] = fsol.doubleField(k);
              } catch (final NumberFormatException ex) {
                System.err.println("IT: " + ex + " \non retourne 0");
                results.pdtSuivants[i].pointsMaillage[j].resultVariables[k - 1] = 0;
              }
            }
          }
        }
      } else {
        results.pdtSuivants = new SPasDeTempsSuivantsSOL[0];
      }
      fsol.close();
    } catch (final IOException ex) {
      System.err.println("IT: " + ex);
    }
    /*
     * catch(EOFException ex){ System.err.println("IT: "+ex) ; }
     */
    return results;
  }
}
