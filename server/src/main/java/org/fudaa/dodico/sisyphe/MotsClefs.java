/*
 * @file         MotsClefs.java
 * @creation     1999-07-09
 * @modification $Date: 2003-11-25 10:11:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sisyphe;
/**
 * @version      $Revision: 1.5 $ $Date: 2003-11-25 10:11:56 $ by $Author: deniger $
 * @author       Mickael Rubens 
 */
public class MotsClefs {
  /**
   * MOTS-CLEFS du fichier CAS (fichier des parametres).
   */
  public static String[] tabMotsClefs=
    new String[] {
      "BANCS DECOUVRANTS",
      "BETA",
      "BILAN DE MASSE",
      "STANDARD DU FICHIER DE GEOMETRIE",
      "STANDARD DU FICHIER PRECEDENT",
      "STANDARD DU FICHIER PRECEDENT SEDIMENTOLOGIQUE",
      "STANDARD DU FICHIER DE REFERENCE",
      "STANDARD DU FICHIER RESULTAT",
      "PAS DE TEMPS",
      "DIAMETRE MOYEN DES GRAINS",
      "GRAVITE",
      "VALEUR MINIMUM DE H",
      "CODE DE CALCUL UTILISE POUR L'HYDRODYNAMIQUE",
      "FORMULE DE TRANSPORT SOLIDE",
      "PERIODE DE SORTIE GRAPHIQUE",
      "PERIODE DE SORTIE LISTING",
      "FICHIER DES PARAMETRES",
      "FICHIER DES CONDITIONS AUX LIMITES",
      "FICHIER FORTRAN",
      "FICHIER DES FONDS",
      "FICHIER DE GEOMETRIE",
      "FICHIER DU CALCUL PRECEDENT",
      "FICHIER PRECEDENT SEDIMENTOLOGIQUE",
      "FICHIER DE REFERENCE",
      "FICHIER DES RESULTATS",
      "NOMBRE DE PAS DE TEMPS",
      "NUMERO DE VERSION",
      "CAS PERMANENT",
      "RAPPORT D'EVOLUTION CRITIQUE",
      "COEFFICIENT DE FROTTEMENT",
      "VARIABLES POUR LES SORTIES GRAPHIQUES",
      "TETA",
      "TITRE",
      "VALIDATION",
      "VARIABLES A IMPRIMER",
      "COEFFICIENT FONCTION DE LA POROSITE",
      "MASSE VOLUMIQUE EAU",
      "MASSE VOLUMIQUE SEDIMENT",
      "ZERO",
      "LOGICIEL DE DESSIN",
      "LOGICIEL DE DESSIN DU CALCUL PRECEDENT",
      "LONGUEUR DU VECTEUR",
      "NOMBRE DE MAREES OU CRUES",
      "NOMBRE DE SOUS-ITERATIONS",
      "COEFFICIENT DE FILTRAGE",
      "OPTION DE TRAITEMENT DES BANCS DECOUVRANTS",
      "PARAMETRE DE SHIELDS",
      "VISCOSITE CINEMATIQUE EAU",
      "BIBLIOTHEQUES",
      "PAS DE TEMPS VARIABLE",
      "USER CRAY",
      "MOT DE PASSE CRAY",
      "TEMPS MACHINE CRAY",
      "PLACE MEMOIRE CRAY",
      "PERIODE DE LA MAREE",
      "MAILLEUR",
      "METHODE DE CALCUL",
      "TEMPS D'ORIGINE DE L'HYDROGRAMME",
      "MASS-LUMPING",
      "SOLVEUR",
      "OPTION DU SOLVEUR",
      "PRECONDITIONNEMENT",
      "MAXIMUM D'ITERATIONS POUR LE SOLVEUR",
      "PRECISION DU SOLVEUR",
      "OPTION DE TRAITEMENT DES FONDS NON ERODABLES" };
  /**
   * ....
   */
  public static String ac= "PARAMETRE DE SHIELDS";
  /**
   * ....
   */
  public static String bandec= "BANCS DECOUVRANTS";
  /**
   * ....
   */
  public static String beta= "BETA";
  /**
   * ....
   */
  public static String bibli= "BIBLIOTHEQUES";
  /**
   * ....
   */
  public static String bilma= "BILAN DE MASSE";
  /**
   * ....
   */
  public static String bingeo= "STANDARD DU FICHIER DE GEOMETRIE";
  /**
   * ....
   */
  public static String binpre= "STANDARD DU FICHIER PRECEDENT";
  /**
   * ....
   */
  public static String binprs= "STANDARD DU FICHIER PRECEDENT SEDIMENTOLOGIQUE";
  /**
   * ....
   */
  public static String binres= "STANDARD DU FICHIER DE REFERENCE";
  /**
   * ....
   */
  public static String binref= "STANDARD DU FICHIER RESULTAT";
  /**
   * ....
   */
  public static String calmeth= "METHODE DE CALCUL";
  /**
   * ....
   */
  public static String delt= "PAS DE TEMPS";
  /**
   * ....
   */
  public static String dm= "DIAMETRE MOYEN DES GRAINS";
  /**
   * ....
   */
  public static String grav= "GRAVITE";
  /**
   * ....
   */
  public static String hmin= "VALEUR MINIMUM DE H";
  /**
   * ....
   */
  public static String hydro= "CODE DE CALCUL UTILISE POUR L'HYDRODYNAMIQUE";
  /**
   * ....
   */
  public static String icf= "FORMULE DE TRANSPORT SOLIDE";
  /**
   * ....
   */
  public static String leopr= "PERIODE DE SORTIE GRAPHIQUE";
  /**
   * ....
   */
  public static String lispr= "PERIODE DE SORTIE LISTING";
  /**
   * ....
   */
  public static String logdes= "LOGICIEL DE DESSIN";
  /**
   * ....
   */
  public static String logpre= "LOGICIEL DE DESSIN DU CALCUL PRECEDENT";
  /**
   * ....
   */
  public static String lump= "MASS-LUMPING";
  /**
   * ....
   */
  public static String lvect= "LONGUEUR DU VECTEUR";
  /**
   * ....
   */
  public static String mailleur= "MAILLEUR";
  /**
   * ....
   */
  public static String maxit= "MAXIMUM D'ITERATIONS POUR LE SOLVEUR";
  /**
   * ....
   */
  public static String memcray= "PLACE MEMOIRE CRAY";
  /**
   * ....
   */
  public static String n1= "COEFFICIENT DE FILTRAGE";
  /**
   * ....
   */
  public static String nmarees= "NOMBRE DE MAREES OU CRUES";
  /**
   * ....
   */
  public static String nomcas= "FICHIER DES PARAMETRES";
  /**
   * ....
   */
  public static String nomcond= "FICHIER DES CONDITIONS AUX LIMITES";
  /**
   * ....
   */
  public static String nomf= "FICHIER FORTRAN";
  /**
   * ....
   */
  public static String nomfon= "FICHIER DES FONDS";
  /**
   * ....
   */
  public static String nomgeo= "FICHIER DE GEOMETRIE";
  /**
   * ....
   */
  public static String nompre= "FICHIER DU CALCUL PRECEDENT";
  /**
   * ....
   */
  public static String nomprs= "FICHIER PRECEDENT SEDIMENTOLOGIQUE";
  /**
   * ....
   */
  public static String nomref= "FICHIER DE REFERENCE";
  /**
   * ....
   */
  public static String nomsol= "FICHIER DES RESULTATS";
  /**
   * ....
   */
  public static String npas= "NOMBRE DE PAS DE TEMPS";
  /**
   * ....
   */
  public static String nsous= "NOMBRE DE SOUS-ITERATIONS";
  /**
   * ....
   */
  public static String numver= "NUMERO DE VERSION";
  /**
   * ....
   */
  public static String optban= "OPTION DE TRAITEMENT DES BANCS DECOUVRANTS";
  /**
   * ....
   */
  public static String optfond= "OPTION DE TRAITEMENT DES FONDS NON ERODABLES";
  /**
   * ....
   */
  public static String optsolv= "OPTION DU SOLVEUR";
  /**
   * ....
   */
  public static String orghydro= "TEMPS D'ORIGINE DE L'HYDROGRAMME";
  /**
   * ....
   */
  public static String pdtvar= "PAS DE TEMPS VARIABLE";
  /**
   * ....
   */
  public static String perma= "CAS PERMANENT";
  /**
   * ....
   */
  public static String pmaree= "PERIODE DE LA MAREE";
  /**
   * ....
   */
  public static String precon= "PRECONDITIONNEMENT";
  /**
   * ....
   */
  public static String psolveur= "PRECISION DU SOLVEUR";
  /**
   * ....
   */
  public static String pwdcray= "MOT DE PASSE CRAY";
  /**
   * ....
   */
  public static String rc= "RAPPORT D'EVOLUTION CRITIQUE";
  /**
   * ....
   */
  public static String sfon= "COEFFICIENT DE FROTTEMENT";
  /**
   * ....
   */
  public static String sortis= "VARIABLES POUR LES SORTIES GRAPHIQUES";
  /**
   * ....
   */
  public static String solveur= "SOLVEUR";
  /**
   * ....
   */
  public static String teta= "TETA";
  /**
   * ....
   */
  public static String titca= "TITRE";
  /**
   * ....
   */
  public static String tmcray= "TEMPS MACHINE CRAY";
  /**
   * ....
   */
  public static String ucray= "USER CRAY";
  /**
   * ....
   */
  public static String valid= "VALIDATION";
  /**
   * ....
   */
  public static String varim= "VARIABLES A IMPRIMER";
  /**
   * ....
   */
  public static String vce= "VISCOSITE CINEMATIQUE EAU";
  /**
   * ....
   */
  public static String xkv= "COEFFICIENT FONCTION DE LA POROSITE";
  /**
   * ....
   */
  public static String xmve= "MASSE VOLUMIQUE EAU";
  /**
   * ....
   */
  public static String xmvs= "MASSE VOLUMIQUE SEDIMENT";
  /**
   * ....
   */
  public static String zero= "ZERO";
}
