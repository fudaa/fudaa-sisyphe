/**
 * @file         DCalculSisyphe.java
 * @creation     1999-07-09
 * @modification $Date: 2006-09-19 14:45:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sisyphe;
import java.io.File;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.sisyphe.ICalculSisyphe;
import org.fudaa.dodico.corba.sisyphe.ICalculSisypheOperations;
import org.fudaa.dodico.corba.sisyphe.IParametresSisyphe;
import org.fudaa.dodico.corba.sisyphe.IParametresSisypheHelper;
import org.fudaa.dodico.corba.sisyphe.IResultatsSisyphe;
import org.fudaa.dodico.corba.sisyphe.IResultatsSisypheHelper;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;
/**
 * @version      $Revision: 1.13 $ $Date: 2006-09-19 14:45:50 $ by $Author: deniger $
 * @author       Mickael Rubens
 */
public class DCalculSisyphe
  extends DCalcul
  implements ICalculSisyphe,ICalculSisypheOperations {
  public DCalculSisyphe() {
    super();
    setFichiersExtensions(new String[] { ".sisyphe_in", ".sisyphe_out" });
  }
  public final  Object clone() throws CloneNotSupportedException {
    return new DCalculSisyphe();
  }
  public String toString() {
    return "DCalculSisyphe()";
  }
  public String description() {
    return "Sisyphe, serveur de calcul pour la sedimentologie"
      + super.description();
  }
  public void calcul(final IConnexion c) {
    if (!verifieConnexion(c)) {
      return;
    }
    final IParametresSisyphe params= IParametresSisypheHelper.narrow(parametres(c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
    }
    final IResultatsSisyphe results= IResultatsSisypheHelper.narrow(resultats(c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
    }
    if ((params.parametresSisypheCOND() == null)
      || (params.parametresSisypheCAS() == null)
      || (params.parametresSisypheGEO() == null)
      || (params.parametresSisypheF() == null)) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
    }
    try {
      final String os= System.getProperty("os.name");
      final String path= cheminServeur();
      log(c, "lancement du calcul");
      final String nomCOND= path + params.parametresSisypheCAS().esFic.noms.nomcond;
      final String nomCAS= path + params.parametresSisypheCAS().esFic.noms.nomcas;
      final String nomF= path + params.parametresSisypheCAS().esFic.noms.nomf;
      final String nomGEO= path + params.parametresSisypheCAS().esFic.noms.nomgeo;
      final String nomREF= path + params.parametresSisypheCAS().esFic.noms.nomref;
      final String nomSOL= path + params.parametresSisypheCAS().esFic.noms.nomsol;
      final File fCOND= new File(nomCOND);
      final File fCAS= new File(nomCAS);
      final File fF= new File(nomF);
      final File fGEO= new File(nomGEO);
      //File fPRE=new File(nomPRE);
      final File fSOL= new File(nomSOL);
      if (fCOND.exists()) {
        fCOND.delete();
      }
      if (fCAS.exists()) {
        fCAS.delete();
      }
      if (fF.exists()) {
        fF.delete();
      }
      if (fGEO.exists()) {
        fGEO.delete();
      }
      //if( fPRE.exists() ) fPRE.delete();
      if (fSOL.exists()) {
        fSOL.delete();
      }
      DParametresSisyphe.ecritParametresCOND(
        nomCOND,
        params.parametresSisypheCOND());
      DParametresSisyphe.ecritParametresCAS(
        nomCAS,
        params.parametresSisypheCAS());
      DParametresSisyphe.ecritParametresGEO(
        nomGEO,
        params.parametresSisypheGEO(),
        1);
      // DParametresSisyphe.ecritParametresPRE(nomPRE, params.parametresSisyphePRE());
      /* Si le fichier ref n'est pas necessaire, alors aucun fichier n'existe,
         et donc nomREF est le repertoire serveurs/sisyphe*/
      if (nomREF.endsWith(".ref")) {
        final File fREF= new File(nomREF);
        if (fREF.exists()) {
          fREF.delete();
        }
        DParametresSisyphe.ecritParametresREF(
          nomREF,
          params.parametresSisypheREF());
      }
      DParametresSisyphe.ecritParametresF(nomF, params.parametresSisypheF());
      System.out.println("Appel de l'executable sisyphe pour l'etude ");
      String[] cmd;
      if (os.startsWith("Windows")) {
        cmd= new String[2];
        cmd[0]= path + "sisyphe-win.bat";
        cmd[1]= path;
      } else {
        cmd= new String[2];
        cmd[0]= path + "sisyphe.sh";
        cmd[1]= path;
      }
      final CExec exec= new CExec();
      exec.setCommand(cmd);
      exec.setOutStream(System.out);
      exec.setErrStream(System.err);
      exec.exec();
      System.out.println("Fin du calcul");
      System.out.println(
        "Le r�sultat du calcul se trouve dans: "
          + params.parametresSisypheCAS().esFic.noms.nomsol);
      log(c, "calcul termin�");
    } catch (final Exception ex) {
      log(c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }
}
